type btr = | L | U of btr | B of btr * btr

let rec btr_of_int i =
  if i = 0 then L
  else B(btr_of_int (pred i), btr_of_int (pred i))

let rec decl = function
  | L -> failwith "L cannot be declemented."
  | U L -> L
  | U d -> U (decl d)
  | B(L, L) -> U L
  | B(L, r) -> U(decl r)
  | B(l, r) -> B(decl l, r)

let rec incr b = function
  | L -> if b () then U L else B(L, L)
  | U d -> U (incr b d)
  | B(l, r) -> B(incr b l, incr b r)

let collect i =
  let rec s_collect acc = function
    | L -> (L::acc)
    | bt -> s_collect (bt::acc) (decl bt) in
  s_collect [] (btr_of_int i)

let rec depth i =
  let s_depth i left =
    List.fold_left
      (fun acc bs ->
        (List.map (fun b -> B(b, left)) bs) @ acc
      ) [] (List.map depth (Util.range 0 i)) in
  if i = 0 then [L]
  else if i = 1 then [U L; B(L, L)]
  else
    (@)
      (List.map (fun d -> U d) (depth (pred i)))
      (List.fold_left (fun acc bs -> acc @ bs) [] (List.map (fun d -> s_depth (pred i) d) (depth (pred i))))

let rec sig_data = function
  | B(l, r) -> (fun (ul, bl) (ur, br) -> (ul + ur, succ (bl + br))) (sig_data l) (sig_data r)
  | U d -> (fun (u, b) -> (succ u, b)) (sig_data d)
  | L -> (0, 0)

let rec btr2exp = function
  | B(l, r) -> List.fold_left (fun acc d -> (fun d' -> Presb.And(d, d'))::(fun d' -> Presb.Or(d, d'))::acc) [] (btr2exp l) |> List.fold_left (fun acc f -> (List.map f (btr2exp r)) @ acc) []
  | U d -> List.fold_left (fun acc d' -> Presb.Exists(0, d')::Presb.Not(d')::acc) [] (btr2exp d)
  | L -> [Presb.Eq("", [], 0)]

let btr2exp_single vars eqs =
  let rec s_btr2exp_single vars eqs = function
    | B(l, r) -> if Random.bool () then Presb.And(s_btr2exp_single vars eqs l, s_btr2exp_single vars eqs r) else Presb.Or(s_btr2exp_single vars eqs l, s_btr2exp_single vars eqs r)
    | U d -> if Random.bool () then Presb.Exists(Random.int vars, s_btr2exp_single vars eqs d) else Presb.Not(s_btr2exp_single vars eqs d)
    | L -> let (n, t, k) = List.nth eqs (Random.int (List.length eqs)) in
           Presb.Eq(n, Array.init (Array.length t) (fun i -> (i, t.(i))) |> Array.to_list, k) in
  begin
    s_btr2exp_single vars eqs
  end

let tk_of_random vars names =
  let i_of_random max =
    if Random.bool () then Random.int max
    else -1 * (Random.int max) in
  let s_tk_of_random vars names =
    List.map (fun n -> (n, Array.init vars (fun _ -> i_of_random 20), i_of_random 100)) names in
  begin
    s_tk_of_random vars names
  end

type algorithm = E of Presb.formula | BothAnd of algorithm * algorithm

let rec has_problems = function
  | E _ -> 1
  | BothAnd(l, r) -> (has_problems l) + (has_problems r)

let return formula = E formula

let to_algorithm = function
  | Presb.Problem(vars, f) -> vars, return f

let rec convert = function
  | E(Presb.Exists(_, a)) -> convert (E a)
  | E(Presb.Or(a, b)) -> BothAnd(convert(E a), convert(E b))
  | E(exp) -> E(convert_t exp)

  (* To be exhaustive *)
  | BothAnd(E a, E b) -> BothAnd(convert(E a), convert(E b))
  | BothAnd(BothAnd(a, a'), E b) -> BothAnd(BothAnd(convert a, convert a'), convert(E b))
  | BothAnd(E a, BothAnd(b, b')) -> BothAnd(convert(E a), BothAnd(convert b, convert b'))
  | BothAnd(BothAnd(a, a'), BothAnd(b, b')) -> BothAnd(BothAnd(convert a, convert a'), BothAnd(convert b, convert b'))
  (* Being exhaustive *)

and convert_t = function
  | Presb.Not(Presb.Not(t)) -> t
  | Presb.Not(Presb.And(a, b)) -> Presb.Or(Presb.Not(a), Presb.Not(b))
  | Presb.Not(Presb.Or(a, b)) -> Presb.And(Presb.Not(a), Presb.Not(b))
  | Presb.Not(Presb.Exists(i, Presb.Not(Presb.And(a, b)))) -> Presb.And(Presb.Not(Presb.Exists(i, Presb.Not(a))), Presb.Not(Presb.Exists(i, Presb.Not(b))))
  (*  | Presb.Not(Presb.Exists(i, Presb.Not(Presb.Or(a, b)))) when false -> Presb.And(Presb.Not(a), Presb.Not(b))*)
  | Presb.Not t -> Presb.Not (convert_t t)
  | Presb.Exists(i, Presb.And(a, b)) when false -> Presb.And(Presb.Exists(i, a), Presb.Exists(i, b))
  | Presb.Exists(i, Presb.Or(a, b)) -> Presb.Or(Presb.Exists(i, a), Presb.Exists(i, b))
  | Presb.Exists(i, t) -> Presb.Exists(i, convert_t t)
  | Presb.And(Presb.Or(a, b), g) | Presb.And(g, Presb.Or(a, b)) -> Presb.Or(Presb.And(a, g), Presb.And(b, g))
  | Presb.And(a, b) -> Presb.And(convert_t a, convert_t b)
  | Presb.Or(a, b) -> Presb.Or(convert_t a, convert_t b)
  | Presb.Eq(_) as eq -> eq

let cv a =
  let rec s_cv prd a = if prd = a then a else s_cv a (convert a) in
  s_cv a (convert a)

let cv_t a =
  let rec s_cv_t prd a = if prd = a then a else s_cv_t a (convert_t a) in
  s_cv_t a (convert_t a)
