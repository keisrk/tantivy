(* Basic Type Definitions *)
type ordering = Greater | Equal | Less
type 'a record = New of 'a | Old of 'a
type 'a tree = L | N of ('a * 'a tree) list

val tr_of_ls : 'a list list -> 'a tree
val ls_of_tr : 'a tree -> 'a list list
val map : ('a -> 'b) -> 'a tree -> 'b tree

val range : int -> int -> int list
val fill : int -> 'a -> 'a list

val dedup : 'a list -> 'a list
val take_while : ('a -> bool) -> 'a list -> 'a list * 'a list
val split3 : ('a * 'b * 'c) list -> 'a list * 'b list * 'c list
val fprintf :
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit 
val json_fprintf :
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit 

val get_op : int -> 'a array -> 'a option

val nth_op : int -> 'a list -> 'a option
val find_op : ('a -> bool) -> 'a list -> 'a option
val assoc_op : 'a -> ('a * 'b) list -> 'b option

val input_line_op : in_channel -> string option
val read : in_channel -> string list

val gen : 'a list -> int -> 'a list list
val pow : int -> int -> int
val int_of_base2 : int list -> int

val filter_map : ('a -> 'b option) -> 'a list -> 'b list
val filter_result : ('a -> (bool, 'b) result) -> 'a list -> ('a list, 'b) result

val lxcmp : ('a -> 'b -> ordering) -> 'a list -> 'b list -> ordering
val of_int : int -> ordering
val of_list : ('a -> 'a -> ordering) -> 'a list -> 'a list
val is_sorted_unique : ('a -> 'a -> ordering) -> 'a list -> bool
val sorted_unique : ('a -> 'a -> ordering) -> 'a -> 'a list -> 'a list 

val ( <:: ) : 'a -> 'a list -> 'a list
val set_compare :
  ('a -> 'a -> ordering) -> 'a list -> 'a list -> ordering option
val union : ('a -> 'a -> ordering) -> 'a list -> 'a list -> 'a list
val inter : ('a -> 'a -> ordering) -> 'a list -> 'a list -> 'a list
val ( <@ ) : 'a list -> 'a list -> 'a list
module Rc :
  sig
    val fprintf :
      (Format.formatter -> 'a -> unit) ->
      Format.formatter -> 'a record -> unit
    val cmp : ('a -> 'a -> ordering) -> 'a record -> 'a record -> ordering
  end

val return : 'a -> 'a record
val unwrap : 'a record -> 'a

val fixpoint : ('a record list -> 'a record list) ->
               ('a -> 'a -> ordering) ->
               ('a -> 'a list) -> 'a record list -> 'a record list

val search : ('a record list -> ('a record list, 'b) result) ->
               ('a -> 'a -> ordering) ->
               ('a -> 'a list) -> 'a record list -> ('a record list, 'b) result

val classify :
  ('a -> 'a -> ordering) -> ('a -> 'a -> bool) -> 'a list -> 'a list list 
