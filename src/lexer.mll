{
open Parser
}

let white = [' ' '\t']
let newline = ['\n']
let int = '-'?['0'-'9']+
let str = ['a'-'z']+

rule problem = parse
| white {problem lexbuf}
| newline {problem lexbuf}
| 'p'  {P}
| '('  {LEFT_BRACE}
| ')'  {RIGHT_BRACE}
| "eq" {ATOM}
| "and" {AND}
| "or" {OR}
| "forall" {FORALL}
| "exists" {EXISTS}
| 'x'      {VAR}
| "not" {NOT}
| '+' {PLUS}

| '=' {EQ}
| ','   {DELIM}
| str  as s {STRING s}
| int  as i {INT (int_of_string i)}
