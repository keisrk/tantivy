let ws_len n_tpl len =
  let unwrap = function | None -> 0 | Some c -> Presb.int_of_char c in
  let ws = Util.gen (Presb.sig_n n_tpl) len |> List.map (fun c -> Presb.make_input_w_op c)
  |> Util.of_list (fun x y -> compare x y |> Util.of_int) in

  let ws_dec = List.map (fun w -> List.mapi (fun i c -> Array.map (fun cc -> (Util.pow 2 i) * (unwrap cc)) c) w
                                  |> List.fold_left (Array.map2 (+)) (Array.make n_tpl 0)) ws in
  (ws, ws_dec)

let check word_set ws_dec term v =
  let init_t = Search.init term in
  let accepted = List.mapi (fun i w -> (i, Search.trans term init_t w)) word_set |> List.filter (fun (_, s) -> List.exists v s) in
  let results = List.map (fun (i, s) -> List.nth ws_dec i) accepted in
  results

let of_ntk sg_n name (t, k) =
  let dlt = Presb.saturate name (List.map Array.of_list sg_n) (t, k) in
  let init = [Atmta.Prim(name ^ "s" ^ (Format.sprintf "%i" k))] in
  let final = [name ^ "s" ^ "0"] in
  (dlt, init, final)

let sig2_readable c =
  let mp = [([|'0'; '0'|], "a"); ([|'0'; '1'|], "b"); ([|'1'; '0'|], "c"); ([|'1'; '1'|], "d")] in
  List.assoc c mp

let sig3_readable c =
  let mp = [([|'0'; '0'; '0'|], "a"); ([|'0'; '0'; '1'|], "b"); ([|'0'; '1'; '0'|], "c"); ([|'0'; '1'; '1'|], "d");
            ([|'1'; '0'; '0'|], "e"); ([|'1'; '0'; '1'|], "f"); ([|'1'; '1'; '0'|], "g"); ([|'1'; '1'; '1'|], "h")] in
  List.assoc c mp

let sig2 = Presb.sig_n 2
let sig3 = Presb.sig_n 3

let sig2_char = List.map (fun cs -> List.map Presb.char_of_int cs |> Array.of_list) sig2
let sig3_char = List.map (fun cs -> List.map Presb.char_of_int cs |> Array.of_list) sig3

let sig2_char_op = List.map (fun cs -> List.map (fun css -> Some(Presb.char_of_int css)) cs |> Array.of_list) sig2
let sig3_char_op = List.map (fun cs -> List.map (fun css -> Some(Presb.char_of_int css)) cs |> Array.of_list) sig3

let layer = [
    ("r", ([|1; 2;-3|], 2)); (*  x + 2y - 3z = 2 *)
    ("b", ([|5;-2; 1|], 3)); (* 5x - 2y +  z = 3 *)
    ("g", ([|3; 1; 2|], 1))  (* 3x +  y + 2z = 1 *)
  ]

let layer2 = [
    ("r", ([|1;-3|], 2)); (*  x  - 3z = 2 *)
    ("b", ([|5; 1|], 3)); (* 5x  +  z = 3 *)
    ("g", ([|3; 2|], 1))  (* 3x  + 2z = 1 *)
  ]

let triangle = [
    ("r", ([|7; -14; 5|], -7)); (*   7x - 14y + 5z = -7 *)
    ("b", ([|-14; 7; 5|], -7)); (* -14x +  7y + 5z = -7 *)
    ("g", ([|14; 14; 1|], 322)) (*  14x + 14y +  z = 322 *)
  ]

let eq2 = [
    ("r", ([|4;   3|], 69)); (*   4x + 3z = 69 *)
    ("b", ([|-1;  3|], 18)); (*  - x + 3z = 18 *)
  ]

let lay () =
  List.map (fun (n, tk) ->
      let (d, i, f) = of_ntk sig3 n tk in
      (Atmta.AtomI(d, i, List.map (fun s -> Atmta.Prim s) f), f, tk)) layer

let lay2 () =
  List.map (fun (n, tk) ->
      let (d, i, f) = of_ntk sig2 n tk in
      (Atmta.AtomI(d, i, List.map (fun s -> Atmta.Prim s) f), f, tk)) layer2

let tri () =
  List.map (fun (n, tk) ->
      let (d, i, f) = of_ntk sig3 n tk in
      (Atmta.AtomI(d, i, List.map (fun s -> Atmta.Prim s) f), f, tk)) triangle

let eq2 () =
  List.map (fun (n, tk) ->
      let (d, i, f) = of_ntk sig2 n tk in
      (Atmta.AtomI(d, i, List.map (fun s -> Atmta.Prim s) f), f, tk)) eq2

let presb2atmta vars (n, (t, k)) =
  let (d, i, f) = of_ntk (Presb.sig_n (vars)) n (t, k) in
  Atmta.AtomI(d, i, List.map (fun s -> Atmta.Prim s) f)

let xy () = [[|x; y|] | x <- Util.range 0 31; y <- Util.range 0 31]
let xyz () = [[|x; y; z|] | x <- Util.range 0 31; y <- Util.range 0 31; z <- Util.range 0 31]
let xyzw () = List.fold_left (fun acc i -> [[|x; y; z; i|] | x <- Util.range 0 31; y <- Util.range 0 31; z <- Util.range 0 31] @acc) [] (Util.range 0 31)            
let candidates_n hd n =
  let candidates = Util.range 0 9999 in
  List.map (fun i -> if i < 10 then hd ^ (string_of_int n) ^ "n000" ^ (string_of_int i)
                     else if i < 100 then hd ^ (string_of_int n) ^ "n00" ^ (string_of_int i)
                     else if i < 1000 then hd ^ (string_of_int n) ^ "n0" ^ (string_of_int i)
                     else hd ^ (string_of_int n) ^ "n" ^ (string_of_int i)) candidates

let eqs_selected hd vars =
  let data = [(2, xy); (3, xyz); (4, xyzw)] in
  let empiricals = [(2, 4); (3, 100); (4, 2100)]in
  let eqs hd vars = Enumerate.tk_of_random vars (candidates_n hd vars )|> List.filter (fun (n, t, k) -> (Array.to_list t |> List.for_all (fun i -> not (i = 0)))) in
  List.filter (fun (n, t, k) -> List.filter (fun a -> (Presb.solve (t, k) a) = 0) ((List.assoc vars data)()) |> List.length > (List.assoc vars empiricals)) (eqs hd vars)

let ps_selected dpt len hd vars =
  let eqs = eqs_selected hd vars in
  let btrs = Enumerate.depth dpt |> List.filter (fun btr -> let (u, b) = Enumerate.sig_data btr in 2 * u > b) in
  let fms = fun _ ->
    let index = Random.int (List.length btrs) in
    (index, Enumerate.btr2exp_single vars eqs (List.nth btrs index)) in
  begin
    Random.init 3;
    List.map fms (Util.range 1 len)
  end

let ps_selected2 dpt len hd vars =
  let eqs = eqs_selected hd vars in
  let btrs = Enumerate.depth dpt |> List.filter (fun btr -> let (u, b) = Enumerate.sig_data btr in 2 * u > b) in
  let fms = fun _ ->
    let index = Random.int (List.length btrs) in
    (index, Enumerate.btr2exp_single vars eqs (List.nth btrs index |> Enumerate.incr Random.bool )) in
  begin
    Random.init 3;
    List.map fms (Util.range 1 len)
  end

let ps_selected3 dpt len hd vars =
  let eqs = eqs_selected hd vars in
  let btrs = Enumerate.depth dpt |> List.filter (fun btr -> let (u, b) = Enumerate.sig_data btr in 2 * u > b) in
  let fms = fun _ ->
    let index = Random.int (List.length btrs) in
    (index, Enumerate.btr2exp_single vars eqs (List.nth btrs index |> Enumerate.incr Random.bool |> Enumerate.incr Random.bool )) in
  begin
    Random.init 3;
    List.map fms (Util.range 1 len)
  end

let rec of_formula vars = function
  | Presb.Eq(n, kv, k) -> let t = Array.init vars (fun i -> List.assoc i kv) in
                          presb2atmta vars (n, (t, k))
  | Presb.And(l, r) -> Atmta.AndI(of_formula vars l, of_formula vars r)
  | Presb.Or(l, r) -> Atmta.SumI(of_formula vars l, of_formula vars r)
  | Presb.Not(f) -> Atmta.NegI(Atmta.DetI(of_formula vars f))
  | Presb.Exists(i, f) -> Atmta.PrjI(i, of_formula vars f)

let of_problem = function
  | Presb.Problem(vars, f) -> of_formula vars f

let rec of_algorithm vars = function
  | Enumerate.E f -> [of_formula vars f]
  | Enumerate.BothAnd(l, r) -> (of_algorithm vars l) @ (of_algorithm vars r)

let of_problem_cv p =
  let vars, algorithm = Enumerate.to_algorithm p in
  of_algorithm vars algorithm

let emptiness_check update vars automaton = match Search.search update (Presb.sig_n vars |> List.map Presb.make_input_c_op) automaton (Search.init automaton) with
  | (Ok fr) -> "Empty", [List.length fr]
  | (Error fr) -> "NonEmpty", [List.length fr]

let emptiness_check_cv update vars automata =
  let rec s_emptiness_check_cv acc vars = function
    | [] -> "Empty", List.rev acc
    | a::xs -> begin match Search.search update (Presb.sig_n vars |> List.map Presb.make_input_c_op) a (Search.init a)  with
               | (Ok fr) -> s_emptiness_check_cv ((List.length fr)::acc) vars xs
               | (Error fr) -> "NonEmpty", (List.length fr::acc) |> List.rev end in
  s_emptiness_check_cv [] vars automata

let take_five () = begin
    Random.init 3;
    List.map (fun _ -> Random.int 500) [0; 1; 2; 3; 4]
  end
