module Var2 = struct
  let range = Util.range 0 31
  let prjct i f = fun x0 x1 -> List.exists (fun c -> if i = 0 then f c x1 else f x0 c) range
  let inter f g = fun x0 x1 -> (f x0 x1) && (g x0 x1)
  let union f g = fun x0 x1 -> (f x0 x1) || (g x0 x1)
  let negtv f = fun x0 x1 -> not(f x0 x1)
  let instance p =
    let rec s_instance = function
      | Presb.Eq(s, t, k) -> fun x0 x1 -> let a = Presb.of_input 2 t in Presb.solve (a, k) [|x0; x1|] = 0
      | Presb.And(l, r) -> inter (s_instance l) (s_instance r)
      | Presb.Or(l, r) -> union (s_instance l) (s_instance r)
      | Presb.Not t -> negtv (s_instance t)
      | Presb.Exists(i, t) -> prjct i (s_instance t) in
    match p with Presb.Problem(_, f) -> s_instance f
end

module Var3 = struct
  let range = Util.range 0 31
  let prjct i f = fun x0 x1 x2 -> List.exists (fun c -> if i = 0 then f c x1 x2 else if i = 1 then f x0 c x2 else f x0 x1 c) range
  let inter f g = fun x0 x1 x2 -> (f x0 x1 x2) && (g x0 x1 x2)
  let union f g = fun x0 x1 x2 -> (f x0 x1 x2) || (g x0 x1 x2)
  let negtv f = fun x0 x1 x2 -> not(f x0 x1 x2)
  let instance p =
    let rec s_instance = function
      | Presb.Eq(_, t, k) -> fun x0 x1 x2 -> let a = Presb.of_input 3 t in Presb.solve (a, k) [|x0; x1; x2|] = 0
      | Presb.And(l, r) -> inter (s_instance l) (s_instance r)
      | Presb.Or(l, r) -> union (s_instance l) (s_instance r)
      | Presb.Not t -> negtv (s_instance t)
      | Presb.Exists(i, t) -> prjct i (s_instance t) in
    match p with Presb.Problem(_, f) -> s_instance f
end
