%token P
%token LEFT_BRACE
%token RIGHT_BRACE
%token ATOM
%token AND
%token OR
%token FORALL
%token EXISTS
%token VAR
%token NOT
%token PLUS

%token EQ
%token PROBLEM

%token DELIM
%token <int>INT
%token <string>STRING

%start <Presb.problem> problem
%%
problem:
  | P; p = INT; f = formula {Problem(p, f)}
formula:
  | ATOM; LEFT_BRACE; n = name; DELIM; ia = int_array; EQ; k = INT; RIGHT_BRACE {Eq (n, ia, k)}
  | AND; LEFT_BRACE; l = formula; DELIM; r = formula; RIGHT_BRACE {And(l, r)}
  | OR; LEFT_BRACE; l = formula; DELIM; r = formula; RIGHT_BRACE {Or(l, r)}
  | NOT; LEFT_BRACE; c = formula; RIGHT_BRACE {Not c}
  | FORALL; LEFT_BRACE; _ = VAR; pos = INT; DELIM; c = formula; RIGHT_BRACE {Not(Exists(pos, Not(c)))}
  | EXISTS; LEFT_BRACE; _ = VAR; pos = INT; DELIM; c = formula; RIGHT_BRACE {Exists(pos, c)}
;
name:
  | n = STRING {n}
  | n = STRING; i = INT {n ^ (string_of_int i)}
  | n = STRING; i = INT; tl = name {n ^ (string_of_int i) ^ tl}
;
int_array:
  | i = INT; _ = VAR ; pos = INT {[(pos, i)]}
  | la = int_array; ra = int_array {la @ ra}
  | la = int_array; PLUS; ra = int_array  {la @ ra}
;