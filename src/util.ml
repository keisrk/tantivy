type ordering = Greater | Equal | Less
type 'a record = New of 'a | Old of 'a
type 'a tree = L | N of ('a * 'a tree) list

let tr_of_ls l =
  let rec s_tr_of_ls = function
    | [] -> L
    | x::xs -> N (List.map (fun c -> (c, s_tr_of_ls xs)) x) in
  begin
    (* List.map f [] never calls f, i.e., [[]; [0; 1]; [0; 1]] returns N [] *)
    assert(List.for_all (fun x -> match x with | [] -> false | s::_ -> true) l);
    s_tr_of_ls l
  end

let ls_of_tr l =
  let rec s_ls_of_tr acc = function
    | L -> [acc]
    | N ctr -> List.fold_left (fun acc' cs -> cs @ acc') [] (List.map (fun (c, tr) -> s_ls_of_tr (c::acc) tr) ctr) in
  s_ls_of_tr [] l

let rec map f = function
  | L -> L
  | N ctr -> N (List.map (fun (c, tr) -> (f c, map f tr)) ctr)

let rec range i j = if i > j then [] else i::(range (succ i) j)

let rec dedup = function
  | [] -> []
  | h::tl -> h::(List.filter (fun x -> x <> h) tl |> dedup)

let take_while p =
  let rec s_takewhile acc p = function
    | [] -> (acc, [])
    | h::tl -> if p h then (fun (acc', rest) -> (h::acc', rest)) (s_takewhile acc p tl)
               else (acc, h::tl) in
  s_takewhile [] p

let split3 l = List.fold_left (fun (acc, bcc, ccc) (a, b, c) -> (a::acc, b::bcc, c::ccc)) ([], [], []) (List.rev l)

let fprintf elemfpf fmt l = begin
    Format.fprintf fmt "@[";
    List.iter (fun x -> Format.fprintf fmt "%a " elemfpf x) l;
    Format.fprintf fmt "@]"
  end

let json_fprintf elemfpf fmt l = begin
    Format.fprintf fmt "[@[";
    List.iteri (fun i x -> if (succ i) = (List.length l) then Format.fprintf fmt "{%a}@," elemfpf x
                           else Format.fprintf fmt "{%a},@," elemfpf x) l;
    Format.fprintf fmt "@]]@,"
  end

let get_op i a = try Array.get a i |> (fun x -> Some x) with | Invalid_argument _ -> None

let nth_op i l = try List.nth l i |> (fun x -> Some x) with | Failure _ -> None
let find_op p l = try List.find p l |> (fun x -> Some x) with | Not_found -> None
let assoc_op k l = try List.assoc k l |> (fun x -> Some x) with | Not_found -> None

let input_line_op ic =
  try input_line ic |> (fun x -> Some x) with
    End_of_file -> begin close_in ic; None end

let rec read ic = match input_line_op ic with
  | None -> []
  | Some s -> s::(read ic)

let fill i a =
  let rec s_fill acc i a = if i = 0 then acc else s_fill (a::acc) (pred i) a in
  s_fill [] i a

let rec succ_l i = function | [] -> [] | x::xs -> if (succ x) mod i = 0 then 0::(succ_l i xs) else (succ x)::xs;;

let rec succ_ll i acc w =
  let w' = succ_l i w in
  if List.for_all (fun i -> i = 0) w' then w'::(List.rev acc)
  else succ_ll i (w'::acc) w'

let gen sg w = List.map (fun l -> List.map (fun i -> List.nth sg i) l) (succ_ll (List.length sg) [] (fill w 0));;

let rec pow a = function
  | 0 -> 1
  | 1 -> a
  | n -> let b = pow a (n / 2) in
         b * b * (if n mod 2 = 0 then 1 else a)

let int_of_base2 l = List.fold_left (+) 0 (List.mapi (fun i c -> (pow 2 i) * c) l)

let filter_map opf =
  let rec s_filter_map acc opf = function
    | [] -> acc
    | x::xs -> match opf x with
               | None -> s_filter_map acc opf xs
               | Some x' -> s_filter_map (x'::acc) opf xs in
  s_filter_map [] opf

let filter_result rsf l =
  let rec s_filter_result acc rsf = function
    | [] -> Ok(List.rev acc)
    | x::xs -> begin match rsf x with
               | Ok(true) -> s_filter_result (x::acc) rsf xs
               | Ok(false) -> s_filter_result acc rsf xs
               | Error(_) as error -> error end in
  s_filter_result [] rsf l

let rec lxcmp cmp l r = match (l, r) with
  | ([], []) -> Equal
  | (l', []) -> Greater
  | ([], r') -> Less
  | (a::l', b::r') -> match cmp a b with
                      | Equal -> lxcmp cmp l' r'
                      | o -> o

let mem_eq cmp a l = List.exists (fun a' -> cmp a a' = Equal) l

let of_int = function
  | 0 -> Equal
  | x when x > 0 -> Greater
  | _ -> Less

let of_list cmp = List.sort_uniq
                (fun a b -> match cmp a b with | Greater -> 1 | Equal -> 0 | Less -> -1 )

let rec is_sorted_unique cmp = function
  | [] | _::[] -> true
  | x::y::tl when cmp x y != Less -> false
  | x::y::tl -> is_sorted_unique cmp (y::tl)

let rec sorted_unique cmp e = function
  | [] -> [e]
  | x::xs as l -> match cmp e x with
                  | Greater -> x::(sorted_unique cmp e xs)
                  | Equal -> l
                  | Less -> e::l

let (<::) e l =
  let cmp x y = compare x y |> of_int in
  begin
    assert (is_sorted_unique cmp l);
    sorted_unique cmp e l end

(* "Set Compare" Assume that x and y are sorted and have no duplicates.

([], Eq, []), (x::xs, Eq, []), ([], Eq, y::ys) => S Eq, S Gr, S Ls
([], Gr, []), (x::xs, Gr, []), ([], Gr, y::ys) => S Gr, S Gr, None
([], Ls, []), (x::xs, Ls, []), ([], Ls, y::ys) => S Ls, None, S Ls
(x::xs, Eq, y::ys) x < y, (x::xs, Eq, y::ys) x = y, (x::xs, Eq, y::ys) y < x 
(x::xs, Gr, y::ys) x < y, (x::xs, Gr, y::ys) x = y, (x::xs, Gr, y::ys) y < x 
(x::xs, Ls, y::ys) x < y, (x::xs, Ls, y::ys) x = y, (x::xs, Ls, y::ys) y < x 
    =>
          rec xs Gr y::ys, rec xs Eq ys, rec x::xs Ls ys
          rec xs Gr y::ys, rec xs Gr ys, None,
          None,            rec xs Ls ys, rec x::xs Ls ys
*)

let set_compare cmp a b =
  let rec scmp cmp a hs b = match (a, hs, b) with
    | ([], Equal, []) -> Some Equal
    | (x::xs, Equal, []) -> Some Greater
    | ([], Equal, y::ys) -> Some Less
    | ([], Greater, []) | (_::_, Greater, [])-> Some Greater
    | ([], Greater, y::ys) -> None
    | ([], Less, []) | ([], Less, _::_)-> Some Less
    | (x::xs, Less, [])-> None
    | (x::xs, Equal, y::ys) -> begin match cmp x y with
                              | Less -> scmp cmp xs Greater (y::ys)
                              | Equal -> scmp cmp xs Equal ys
                              | Greater -> scmp cmp (x::xs) Less ys end
    | (x::xs, Greater, y::ys) -> begin match cmp x y with 
                              | Less -> scmp cmp xs Greater (y::ys)
                              | Equal -> scmp cmp xs Greater ys
                              | Greater -> None end
    | (x::xs, Less, y::ys) -> begin match cmp x y with
                              | Less -> None
                              | Equal -> scmp cmp xs Less ys
                              | Greater -> scmp cmp (x::xs) Less ys end in
  begin
    assert (is_sorted_unique cmp a);
    assert (is_sorted_unique cmp b);
    scmp cmp a Equal b
  end

let union cmp a b =
  let rec s_union cmp a b = match (a, b) with
    | ([], []) -> [] | (x, []) -> x | ([], y) -> y
    | (x::xs, y::ys) -> begin match cmp x y with
                        | Less -> x::(s_union cmp xs (y::ys))
                        | Equal -> x::(s_union cmp xs ys)
                        | Greater -> y::(s_union cmp (x::xs) ys) end in
  begin
    assert (is_sorted_unique cmp a);
    assert (is_sorted_unique cmp b);
    s_union cmp a b end

let inter cmp a b =
  let rec s_inter cmp a b = match (a, b) with
    | ([], []) -> [] | (x, []) -> [] | ([], y) -> []
    | (x::xs, y::ys) -> begin match cmp x y with
                        | Less -> s_inter cmp xs (y::ys)
                        | Equal -> x::(s_inter cmp xs ys)
                        | Greater -> s_inter cmp (x::xs) ys end in
  begin
    assert (is_sorted_unique cmp a);
    assert (is_sorted_unique cmp b);
    s_inter cmp a b end

let (<@) a b =
  let cmp x y = compare x y |> of_int in
  begin
    assert (is_sorted_unique cmp a);
    assert (is_sorted_unique cmp b);
    union cmp a b end



module Rc =
  struct
    let fprintf elemfpf fmt = function
      | New n -> Format.fprintf fmt "@,@[New(%a)@]@," elemfpf n
      | Old o -> Format.fprintf fmt "@,@[Old(%a)@]@," elemfpf o

    let cmp elemcmp x y = match (x, y) with
      | (New n, New n') -> elemcmp n n'
      | (Old o, Old o') -> elemcmp o o'
      | (New n, Old o) -> Less
      | (Old o, New n) -> Greater
  end

let bind a = New a
let return a = New a
let unwrap = function
  | New n -> n | Old o -> o

let fixpoint update cmp f l =
  let (<::) e l =
    begin
      assert (is_sorted_unique (Rc.cmp cmp) l);
      sorted_unique (Rc.cmp cmp) e l end in
  let (<@) a b =
    begin
      assert (is_sorted_unique (Rc.cmp cmp) a);
      assert (is_sorted_unique (Rc.cmp cmp) b);
      union (Rc.cmp cmp) a b end in
  let s_fixpoint acc f = function
    | New n -> ((Old n) <::(List.map return (f n))) <@ acc |> update
    | Old o -> acc in
  List.fold_left (fun acc rc -> s_fixpoint acc f rc) l l

let search update cmp f l =
  let (<::) e l =
    begin
      assert (is_sorted_unique (Rc.cmp cmp) l);
      sorted_unique (Rc.cmp cmp) e l end in
  let (<@) a b =
    begin
      assert (is_sorted_unique (Rc.cmp cmp) a);
      assert (is_sorted_unique (Rc.cmp cmp) b);
      union (Rc.cmp cmp) a b end in
  let s_search acc f = function
    | New n -> ((Old n) <::(List.map return (f n))) <@ acc |> update
    | Old o -> Ok( acc ) in
  List.fold_left (fun acc rc -> match acc with
                                | Error(_) as error -> error
                                | Ok(fr) -> s_search fr f rc) (Ok(l)) l

let classify cmp p l =
  let mat = List.map (fun e -> List.fold_right (fun e' acc -> if p e e' then e'<::acc else acc) l []) l in
  begin
    assert (List.for_all (fun x -> List.exists (fun y -> p x y) l) l);
    List.filter (fun r -> if List.exists (fun r' -> set_compare cmp r' r = (Some Less)) mat then false else true) mat
  end
