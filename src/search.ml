let alt_arr a i c = Array.init (Array.length a) (fun i' -> if i = i' then c else a.(i'))
let rec init = function
  | Atmta.AtomI(d, s, f) -> s
  | Atmta.AndI(l, r) -> let sl = init l in
                        let sr = init r in
                        [Atmta.Prod(a, b) | a <- sl; b <- sr]
  | Atmta.SumI(l, r) -> let sl = init l in
              let sr = init r in
              Atmta.St8.([Atmta.Sum (Atmta.Lhs s) | s <- sl] <@ [Atmta.Sum (Atmta.Rhs s) | s <- sr])
  | Atmta.DetI c -> (fun s -> [Atmta.M s]) (init c)
  | Atmta.PrjI(i, c) -> init c
  | Atmta.NegI c -> init c
  |_ -> failwith "Under Construction"

let rec delta q c = function
  | Atmta.AtomI(d, _, _) -> Atmta.delta d q c
  | Atmta.AndI(l, r) -> begin match q with
                        | Atmta.Prod(ql, qr) ->
                           let sl = delta ql c l in
                           let sr = delta qr c r in
                           [Atmta.Prod(pl, pr) | pl <- sl; pr <- sr]
                        | _ -> failwith "Atmta.compos_post Other than Atmta.Prod" end
  | Atmta.SumI(l, r) -> begin match q with
                        | Atmta.Sum (Atmta.Lhs p) -> List.map (fun q' -> Atmta.Sum (Atmta.Lhs q')) (delta p c l)
                        | Atmta.Sum (Atmta.Rhs p) -> List.map (fun q' -> Atmta.Sum (Atmta.Rhs q')) (delta p c r)
                        | _ -> failwith "Atmta.compos_post Other than Sum" end
  | Atmta.DetI t -> begin match q with
                    | Atmta.M ss -> begin
                        let sss = List.fold_left (fun acc p -> Atmta.St8.((delta p c t) <@ acc)) [] ss in
                                     assert(not (ss = []));
                                     assert(not (sss = []));
                                     [Atmta.M sss] end
                    | _ -> failwith "Atmta.compos_post Other than M" end
  | Atmta.PrjI(i, t) -> List.fold_left (fun acc cc -> Atmta.St8.((delta q (alt_arr c i cc) t) <@ acc)) [] [Some '0'; Some '1']
  | Atmta.NegI t -> delta q c t
  |_ -> failwith "Under Construction"

let trans d s w =
  let rec s_trans acc d = function
    | [] -> acc
    | x::xs -> s_trans (List.fold_left (fun acc q -> Atmta.St8.((delta q x d) <@ acc)) [] acc) d xs in
  s_trans s d w

let post_incr sg t p =
  let update l =
    List.filter (
        fun rc -> match rc with
                  (* List.mem [|None; ..|] causes the trouble *)
                  | Util.New n -> if List.mem (Util.Old n) l then false else true
                  | Util.Old _ -> true
      ) l in
  let f  = function
    | q -> List.fold_left (fun acc c -> Atmta.St8.((delta q c t) <@ acc)) [] sg in
  Util.fixpoint update Atmta.St8.cmp f p

let reach sg t s =
  let rec s_reach sg t p =
    let fr = post_incr sg t p in
    if List.for_all (fun x -> match x with | Util.Old _ -> true | _ -> false) fr then fr
    else s_reach sg t fr in
  s_reach sg t (List.map Util.return s)

let witness t q =
  if Atmta.is_accept t q then Error(q)
  else Ok(q)

let update t l =
  Util.filter_result (
      fun rc -> match rc with
                (* List.mem [|None; ..|] causes the trouble *)
                | Util.New n -> begin match witness t n with
                                | Error(_) -> Error(l)
                                | Ok(q) -> if List.mem (Util.Old q) l then Ok(false)
                                           else Ok(true) end
                | Util.Old _ -> Ok(true)
    ) l

let search_incr update sg t p =
  let f  = function
    | q -> List.fold_left (fun acc c -> Atmta.St8.((delta q c t) <@ acc)) [] sg in
  Util.search (update t) Atmta.St8.cmp f p

let search update sg t s =
  let rec s_search update sg t p =
    match search_incr update sg t p with
    | Ok(fr) -> if List.for_all (fun x -> match x with | Util.Old _ -> true | _ -> false) fr
                then Ok(fr) else s_search update sg t fr
    | Error _ as error -> error in

  (* To Check Initial States are already Accepting *)
  match List.filter (fun q -> Atmta.is_accept t q) s with
  | [] -> s_search update sg t (List.map Util.return s)
  | x -> Error(List.map Util.return x)

let rec cmp_op t p q = match t with
  | Atmta.AtomI _ -> if p = q then Some Util.Equal else None
  | Atmta.AndI(l, r) -> begin match p, q with
                        | Atmta.Prod(pl, pr), Atmta.Prod(ql, qr) ->
                           begin match cmp_op l pl ql, cmp_op r pr qr with
                           | None, None | None, _ | _, None -> None
                           | Some Util.Equal, o | o, Some Util.Equal -> o
                           | Some Util.Less, Some Util.Less -> Some Util.Less
                           | Some Util.Less, Some Util.Greater | Some Util.Greater, Some Util.Less -> None
                           | Some Util.Greater, Some Util.Greater -> Some Util.Greater end
                        | _ -> failwith "Other than Prod" end
  | Atmta.SumI(l, r) -> begin match p, q with
                      | Atmta.Sum(Atmta.Lhs p'), Atmta.Sum(Atmta.Lhs q') -> cmp_op l p' q'
                      | Atmta.Sum(Atmta.Rhs p'), Atmta.Sum(Atmta.Rhs q') -> cmp_op r p' q'
                      | Atmta.Sum(_), Atmta.Sum(_) -> None
                      | _ -> failwith "Other than Sum" end
  | Atmta.PrjI(_, t') -> cmp_op t' p q
  | Atmta.NegI(Atmta.DetI(t')) -> begin match p, q with
                                  | Atmta.M s, Atmta.M u -> Util.set_compare Atmta.St8.cmp s u
                                  |_ -> failwith "Other than M" end
  | _ -> failwith "DetI not followed by NegI"

let is_subsumed_by t p q = match cmp_op t p q with
  | Some Util.Equal | Some Util.Greater -> true
  | _ -> false

let update_ord t l =
  Util.filter_result (
      fun rc -> match rc with
                | Util.New n -> begin match witness t n with
                                | Error(_) -> Error(l)
                                | Ok(q) -> if List.exists (fun rc' -> match rc' with
                                                                      | Util.Old p -> is_subsumed_by t q p
                                                                      | Util.New _ -> false ) l then Ok(false)
                                           else Ok(true) end
                | Util.Old _ -> Ok(true)
    ) l
