let () =
  let lexbuf = Lexing.from_channel stdin in
  let problem = Parser.problem Lexer.problem lexbuf in
  let vars, algorithm = (fun (v, a) -> v, Enumerate.cv a) (Enumerate.to_algorithm problem) in
  let free = Presb.free_vars problem in
  let sub_problems = Enumerate.has_problems algorithm in
  begin
    Format.fprintf Format.std_formatter "\"free\": %a,@," (Util.json_fprintf (fun fmt i -> Format.fprintf fmt "\"variable\": %i@," i)) free;
    Format.fprintf Format.std_formatter "\"subs\": %i,@," sub_problems;
  end
