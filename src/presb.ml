type problem = Problem of int * formula
 and formula = Eq of string * ((int * int) list) * int | And of formula * formula | Or of formula * formula | Not of formula | Exists of int * formula
let has_vars = function | Problem (i, _) -> i

let of_input vars kv = Array.init vars (fun i -> match Util.assoc_op i kv with | None -> 0 | Some i' -> i')

let int_fprintf flag fmt i =
  if flag  then begin
      Format.fprintf fmt " %i" i end
  else begin
      if i < 0 then Format.fprintf fmt " %i" i else Format.fprintf fmt "+ %i" i end

let arr_fprintf fmt a =
  let l = Array.to_list a |> List.mapi (fun i c -> (i, c)) in
  Util.fprintf (fun fmt (i, c) -> Format.fprintf fmt "%ax%i" (int_fprintf (i = 0)) c i) fmt l

let fprintf fmt p =
  let rec f_fprintf vars fmt = function
    | Eq(s, t, k) -> Format.fprintf fmt "eq(%s,%a= %i)" s arr_fprintf (of_input vars t) k
    | And(l, r) -> Format.fprintf fmt "and(@[%a,@, %a@])" (f_fprintf vars) l (f_fprintf vars) r
    | Or(l, r) -> Format.fprintf fmt "or(@[%a,@, %a@])" (f_fprintf vars) l (f_fprintf vars) r
    | Not f -> Format.fprintf fmt "not(%a)" (f_fprintf vars) f
    | Exists(i, f) -> Format.fprintf fmt "exists(x%i,@, %a)" i (f_fprintf vars) f
    | _ -> () in
  match p with Problem(i, f) -> Format.fprintf fmt "p %i @,@[%a@]@." i (f_fprintf i) f

let sg_n n = List.map (fun _ -> [0; 1]) (Util.range 1 n)
let sig_n n_tpl = sg_n n_tpl |> Util.tr_of_ls |> Util.ls_of_tr
                  |> Util.of_list (fun x y -> compare x y |> Util.of_int) 

let char_of_int i = if i > 9 then failwith "char_of_int: Greater than 9" else String.get (string_of_int i) 0
let int_of_char c = int_of_string (String.make 1 c)

let fixpoint =
  let update l =
  List.filter (
      fun rc -> match rc with
                  (* List.mem [|None; ..|] causes the trouble *)
                | Util.New n -> if List.mem (Util.Old n) l then false else true 
                | Util.Old _ -> true
    ) l in
  Util.fixpoint update

let solve (a, k) a' =
  Array.map2 (fun x y -> x * y) a a'
  |> Array.fold_left (fun acc x -> acc + x) 0
  |> fun x -> (k - x)

let init sg (t, k) =
  List.map (fun e ->
      let k' = solve (t, k) e in
      if k' mod 2 = 0 then ((t, Some k), e, (t, Some (k'/2)))
      else ((t, Some k), e, (t, None))) sg

let satur_incr sg prd =
  let f = function
    |(_, _, (t, None)) ->
      List.map (fun e -> (t, None), e, (t, None)) sg
    |(_, _, (t, Some k)) ->
      List.map (fun e ->
          let k' = solve (t, k) e in
          if k' mod 2 = 0 then ((t, Some k), e, (t, Some (k'/2)))
          else ((t, Some k), e, (t, None))) sg
      |> Util.of_list (fun x y -> compare x y |> Util.of_int) in
  fixpoint (fun x y -> compare x y |> Util.of_int) f prd

let to_string_op name = function
  | (t, None) -> name ^ "s" ^ "_"
  | (t, Some k) -> name ^ "s" ^ (Format.sprintf "%i" k)

let to_d_op name r = List.map (fun (a, c, a') ->
                         ((to_string_op name a), Array.map (fun x -> Some (char_of_int x)) c, (to_string_op name a'))) r

let saturate n sg (t, k) =
  let rec s_saturate prd =
    let fr = satur_incr sg prd  in
    if List.for_all (fun x -> match x with | Util.Old _ -> true | _ -> false) fr then fr
    else s_saturate fr in

  let init_d = init sg (t, k) |> Util.of_list (fun x y -> compare x y |> Util.of_int) |> List.map Util.return in
  let satur_d = s_saturate init_d |> List.map Util.unwrap in

  to_d_op n satur_d |> List.map (fun (s, c, s') -> (Atmta.Prim s, c, Atmta.Prim s'))
  |> Util.of_list Atmta.Dlt.cmp

let make_input_c l = List.map (fun i -> char_of_int i) l |> Array.of_list
let make_input_w = List.map make_input_c

let make_input_c_op l = List.map (fun i -> Some (char_of_int i)) l |> Array.of_list
let make_input_w_op = List.map make_input_c_op


let rec f_free_vars = function
  | Eq(_, t, _) -> List.fold_right (fun (k, v) acc -> if v = 0 then acc else Util.(k<::acc)) t []
  | And(l, r) | Or(l, r) -> Util.((f_free_vars l) <@ (f_free_vars r))
  | Not t -> f_free_vars t
  | Exists(i, t) -> List.filter (fun i' -> if i = i' then false else true) (f_free_vars t)

let free_vars = function
  | Problem(_, f) -> f_free_vars f
