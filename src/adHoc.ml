let (<::) e l =
  begin
    assert (Util.is_sorted_unique (Util.Rc.cmp Atmta.St8.cmp) l);
    Util.sorted_unique (Util.Rc.cmp Atmta.St8.cmp) e l end

(* Assume that we are computing Emptiness Check. *)

let case_universality4_r t s =
  let f ss = match ss with
    | Util.New(Atmta.M(s')) | Util.Old(Atmta.M(s')) -> s'
    | _ -> failwith "universality_check: Other than M" in
  let rec shrink l = function
    | [] -> []
    | (Util.New(Atmta.M(ss)))::xs ->
       if List.exists (fun rc -> Util.set_compare Atmta.St8.cmp ss (f rc) = (Some Util.Less)) l then
         (Util.Old(Atmta.M(ss))) <:: (shrink l xs)
       else (Util.New(Atmta.M(ss)))::(shrink l xs)
    | x::xs -> x::(shrink l xs) in

  match t with (* t is empty iff r is universal *)
  | Atmta.NegI(Atmta.DetI(r)) -> shrink s s
  | _ -> s

let case_inclusion4_lr t s =
  let f ss = match ss with
    | Util.New(Atmta.Prod(q, Atmta.M(s'))) | Util.Old(Atmta.Prod(q, Atmta.M(s'))) -> (q, s')
    | _ -> failwith "inclusion_check: Other than Prod(_, M_)" in
  let rec shrink l = function
    | [] -> []
    | (Util.New(Atmta.Prod(q, Atmta.M(ss))))::xs ->
       if List.exists (fun rc -> let q', ss' = f rc in
                                 (&&) (Util.set_compare Atmta.St8.cmp ss ss' = (Some Util.Less))
                                      (q = q')) l then
         (Util.Old(Atmta.Prod(q, Atmta.M(ss)))) <:: (shrink l xs)
       else (Util.New(Atmta.Prod(q, Atmta.M(ss))))::(shrink l xs)
    | x::xs -> x::(shrink l xs) in

  match t with (* t is empty iff l subseteq r *)
  | Atmta.AndI(l, Atmta.NegI(Atmta.DetI(r))) -> shrink s s
  | _ -> s
