let depth, length, header, variables = int_of_string Sys.argv.(1), int_of_string Sys.argv.(2), Sys.argv.(3), int_of_string Sys.argv.(4)
let () =
  let ps_selected = if depth < 5 then List.map (fun (n, f) -> (n, Presb.Problem(variables, f))) (Testset.ps_selected depth length header variables)
                    else if depth < 6 then List.map (fun (n, f) -> (n, Presb.Problem(variables, f))) (Testset.ps_selected2 (pred depth) length header variables)
                    else List.map (fun (n, f) -> (n, Presb.Problem(variables, f))) (Testset.ps_selected3 (pred (pred depth)) length header variables) in
  Util.fprintf (fun fmt (n, f) ->
      begin Format.fprintf fmt "#%i\n" n;
            Presb.fprintf fmt f end) Format.std_formatter ps_selected
