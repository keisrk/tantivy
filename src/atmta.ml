type 'a sum = Lhs of 'a | Rhs of 'a
type st8 = Prim of string | Prod of st8 * st8 | M of st8 list | Sum of st8 sum

type compose_term = AtomI of ((st8 * char option array * st8) list) * (st8 list) * (st8 list)
                  | AndI of compose_term * compose_term
                  | SumI of compose_term * compose_term
                  | DetI of compose_term
                  | PrjI of int * compose_term
                  | NegI of compose_term

module Sum =
  struct
    let fprintf elemfpf fmt = function
      | Lhs l -> Format.fprintf fmt "@,@[L(%a)@]@," elemfpf l
      | Rhs r -> Format.fprintf fmt "@,@[R(%a)@]@," elemfpf r

    let cmp elemcmp x y = match (x, y) with
      | (Lhs _, Rhs _) -> Util.Less
      | (Rhs _, Lhs _) -> Util.Greater
      | (Lhs a, Lhs b) | (Rhs a, Rhs b) -> elemcmp a b
  end

module St8 =
  struct
    let rec fprintf fmt = function
      | Prim s -> Format.fprintf fmt "%s@," s
      | Prod(l, r) -> Format.fprintf fmt "(%a, %a)@," fprintf l fprintf r
      | M l -> begin
          Format.fprintf fmt "@,@[{ ";
          List.iter (fun s -> Format.fprintf fmt "%a " fprintf s) l;
          Format.fprintf fmt "}@]@,"
        end
      | Sum s -> Format.fprintf fmt "%a@," (Sum.fprintf fprintf) s

    let rec cmp x y = match (x, y) with
      | (Prim x', Prim y') -> String.compare x' y' |> Util.of_int
      | (Prod(l, r), Prod(l', r')) -> begin match cmp l l' with
                                      | Equal -> cmp r r'
                                      | o -> o end
      | (M l, M r) -> Util.lxcmp cmp l r
      | (Sum l, Sum r) -> Sum.cmp cmp l r
      | x, y -> begin
          Format.fprintf Format.std_formatter "\nx is %a\n" fprintf x;
          Format.fprintf Format.std_formatter "\ny is %a\n" fprintf y;
          failwith "St8.cmp: Invalid_arguments";
        end

    let rec to_string = function
      | Prim s -> s
      | Prod (s, s') -> Format.sprintf "(%s, " (to_string s) ^ Format.sprintf "%s)" (to_string s')
      | M l -> List.fold_right (fun s acc -> (to_string s)^", " ^ acc) l ""
      | Sum (Lhs s) | Sum (Rhs s) -> to_string s

    let (<::) e l =
      begin
        assert (Util.is_sorted_unique cmp l);
        Util.sorted_unique cmp e l end

    let (<@) a b =
      begin
        assert (Util.is_sorted_unique cmp a);
        assert (Util.is_sorted_unique cmp b);
        Util.union cmp a b end
    let rec is_accept finals = function
      | Prim str -> List.mem str finals
      | Prod (l, r) -> is_accept finals l && is_accept finals r
      | Sum (Lhs s) | Sum (Rhs s) -> is_accept finals s
      | M ss -> List.exists (is_accept finals) ss
  end

module Chr =
  struct
    let fprintf fmt a =
      Format.fprintf fmt "%s" (Array.fold_right (fun s acc -> s ^ acc)(Array.map (fun x -> match x with | None -> "#" | Some x' -> String.make 1 x' ) a) "")

    let cmp a b =
      begin
        assert (Array.length a = Array.length b);
        compare a b |> Util.of_int
      end

(* Under Sigma ['0', '1'], [| None; Some '0'; Some '1'|] represents both [| Some '0'; Some '0'; Some '1'|] and [| Some '1'; Some '0'; Some '1'|]. 
Let 0 < i < n, where n is (Array.length a)

(None, Eq, None), (Some c, Eq, None), (None, Eq, Some c), (Some c, Eq, Some c), (Some x, Eq, Some y) => rec i++ a Eq b, rec i++ a Ls b, rec i++ a Gr b, rec i++ a Eq b, None
(None, Gr, None), (Some c, Gr, None), (None, Gr, Some c), (Some c, Gr, Some c), (Some x, Gr, Some y) => rec i++ a Gr b, None,           rec i++ a Gr b, rec i++ a Gr b, None
(None, Ls, None), (Some c, Ls, None), (None, Ls, Some c), (Some c, Ls, Some c), (Some x, Ls, Some y) => rec i++ a Ls b, rec i++ a Ls b, None,           rec i++ a Ls b, None *)

    let rpr_cmp a b =
      let rec s_rpr_cmp j i a ord b =
        if j = i then Some ord
        else match (a.(i), ord, b.(i)) with
             | (None, Util.Equal, None) -> s_rpr_cmp j (succ i) a Util.Equal b
             | (Some _, Util.Equal, None) -> s_rpr_cmp j (succ i) a Util.Less b
             | (None, Util.Equal, Some _) -> s_rpr_cmp j (succ i) a Util.Greater b
             | (Some x, Util.Equal, Some y) when x = y -> s_rpr_cmp j (succ i) a Util.Equal b

             | (None, Util.Greater, None) -> s_rpr_cmp j (succ i) a Util.Greater b
             | (Some _, Util.Greater, None) -> None
             | (None, Util.Greater, Some _) -> s_rpr_cmp j (succ i) a Util.Greater b
             | (Some x, Util.Greater, Some y) when x = y -> s_rpr_cmp j (succ i) a Util.Greater b

             | (None, Util.Less, None) -> s_rpr_cmp j (succ i) a Util.Less b
             | (Some _, Util.Less, None) -> s_rpr_cmp j (succ i) a Util.Less b
             | (None, Util.Less, Some _) -> None
             | (Some x, Util.Less, Some y) when x = y -> s_rpr_cmp j (succ i) a Util.Less b

             | (Some _, _, Some _) -> None in
      begin
        assert (Array.length a = Array.length b);
        s_rpr_cmp (Array.length a) 0 a Util.Equal b
      end

    let is_represented_by a b = match rpr_cmp a b with
      | None | Some Util.Greater -> false
      | Some Util.Equal | Some Util.Less -> true
  end

module Dlt =
  struct
    let fprintf fmt (a, c, a') =
        Format.fprintf fmt "@,@[%a--%a->%a @]@," St8.fprintf a Chr.fprintf c St8.fprintf a'

    let cmp (a, c, a') (b, c', b') = match St8.cmp a b with
      | Util.Equal -> begin match Chr.cmp c c'(*compare c c' |> Util.of_int*) with
                      | Util.Equal -> St8.cmp a' b'
                      | o -> o end
      | o -> o

    let (<::) e l =
      begin
        assert (Util.is_sorted_unique cmp l);
        Util.sorted_unique cmp e l end

    let (<@) a b =
      begin
        assert (Util.is_sorted_unique cmp a);
        assert (Util.is_sorted_unique cmp b);
        Util.union cmp a b end
  end

let rec fprintf fmt = function
  | AtomI(d, s, f) -> Format.fprintf fmt "%a@," (Util.fprintf St8.fprintf) s
  | AndI(l, r) -> Format.fprintf fmt "(@[%a (x) %a@])@," fprintf l fprintf r
  | SumI(l, r) -> Format.fprintf fmt "(@[%a (+) %a@])@," fprintf l fprintf r
  | DetI t -> Format.fprintf fmt "%a.d@," fprintf t
  | PrjI(i, t) -> Format.fprintf fmt "%a.p%i@," fprintf t i
  | NegI t -> Format.fprintf fmt "%a.c@," fprintf t

(* val AdHoc.is_accept: compose_init -> st8 list -> bool *)
let rec is_accept t q = match t with
  | AtomI(d, _, finals) -> begin match q with
                           | Prim s' as pr -> List.mem pr finals
                           | _ -> failwith "is_accept: Other than Prim" end
  | AndI(lt, rt) -> begin match q with
                    | Prod(lq, rq) -> (is_accept lt lq) && (is_accept rt rq)
                    | _ -> failwith "is_accept: Other than Prod" end
  | SumI(lt, rt) -> begin match q with
                            | Sum(Lhs lq) -> is_accept lt lq
                            | Sum(Rhs rq) -> is_accept rt rq
                            | _ -> failwith "is_accept: Other than Sum" end
  | DetI t' -> begin match q with
                       | M s -> List.exists (fun q' -> is_accept t' q') s
                       | _ -> failwith "is_accept: Other than M" end
  | PrjI(_, t')-> is_accept t' q
  | NegI t' -> not (is_accept t' q)

let pi i a =
  let a' = Array.copy a in
  begin
    Array.set a' i None ; a' end

let is_equal a b = match (a, b) with
  | (None, _) | (_, None) -> true
  | (Some a', Some b') -> a' = b'

let pseudo_equal a b = match (a, b) with
  | (None, _) -> true
  | (Some a', b') -> a' = b'

let for_all2 p a b =
  let rec s_for_all2 p i j a b =
    if i = j then true
    else if p a.(i) b.(i) then s_for_all2 p (succ i) j a b
    else false in
  begin
    assert (Array.length a = Array.length b);
    s_for_all2 p 0 (Array.length a) a b end

let subsume a b = match (a, b) with
  | (Some a', Some b') -> if a' = b' then a else failwith "subsume failed"
  | (None, None) -> None
  | (o, None) | (None, o) -> o

let comb a b = Array.map2 subsume a b

let comb_fold = function
  | [] -> failwith "comb_fold of empty list"
  | x::xs as l -> List.fold_left (fun acc b -> comb acc b) (Array.make (Array.length x) None) l

let (~=) a b = for_all2 is_equal a b
let is_eq a b = for_all2 is_equal a b
let (~~) a b = for_all2 pseudo_equal a b
let is_pseq a b = for_all2 pseudo_equal a b

let rec is_det = function
  | [] -> true
  | (q', c', p')::xs ->
     if List.exists (fun (q, c, p) -> q = q' && is_eq c c' && p != p') xs then false else is_det xs

let delta d q c =
  let rec s_delta acc q c = function
    | [] -> acc
    | (q', c', p')::xs when is_eq c' c && q = q' -> s_delta St8.(p'<::acc) q c xs
    | _::xs -> s_delta acc q c xs in
  s_delta [] q c d

let post d s c =
  let rec s_post acc s c = function
    | [] -> acc
    | (q', c', p')::xs when is_pseq c' c && List.mem q' s -> s_post St8.(p'<::acc) s c xs
    | _::xs -> s_post acc s c xs in
  s_post [] s c d

let post_op d s c =
  let rec s_post_op acc s c = function
    | [] -> acc
    | (q', c', p')::xs when is_eq c' c && List.mem q' s -> s_post_op St8.(p'<::acc) s c xs
    | _::xs -> s_post_op acc s c xs in
  s_post_op [] s c d

let collect d s =
  let rec s_collect acc s = function
    | [] -> acc
    | (q', _, p')::xs when List.mem q' s -> s_collect St8.(p'<::acc) s xs
    | _::xs -> s_collect acc s xs in
  s_collect [] s d

let reach d s i =
  let rec s_reach acc d = function
    | 0 -> acc
    | i -> s_reach St8.((collect d acc) <@ acc) d (pred i) in
  s_reach s d i

let trans d s w =
  let rec s_trans acc d = function
    | [] -> acc
    | x::xs -> s_trans (post d acc x) d xs in
  s_trans s d w
