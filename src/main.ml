let convert_on = Array.mem "-c" (Sys.argv)
let antichain_on = Array.mem "-a" (Sys.argv)

let update = if antichain_on then Search.update_ord
             else Search.update

let emptiness_check = if convert_on then (fun p -> Testset.emptiness_check_cv update (Presb.has_vars p) (Testset.of_problem_cv p))
                      else (fun p -> Testset.emptiness_check update (Presb.has_vars p) (Testset.of_problem p))
let () =
  let lexbuf = Lexing.from_channel stdin in
  let problem = Parser.problem Lexer.problem lexbuf in
  let result, size = emptiness_check problem in
  begin
    Format.fprintf Format.std_formatter "\"answer\": \"%s\",@," result;
    Format.fprintf Format.std_formatter "\"total\": %a,@,"(Util.json_fprintf (fun fmt i -> Format.fprintf fmt "\"size\": %i@," i)) size;
    Format.fprintf Format.std_formatter "\"time\": %f,@," (Sys.time ());
  end
