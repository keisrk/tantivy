(* Input;
   - argv1 := problem file 
   - argv2 := blacklist file, or explicitly specify "None"
   - argv3, argv3 := "-c", "-a"
*)

let timeout = "5m"
let convert_on = Array.mem "-c" Sys.argv
let antichain_on = Array.mem "-a" Sys.argv

(* Configuration;
   - timeout: string ::= "2s" | "5m" | "1h"
   - convert_on: string ::= "-c" | ""
   - antichain_on: string ::= "-a" | ""
*)

let take_comment s = match s.[0] with
  | '#' -> Some (String.init (pred(String.length s)) (fun i -> s.[succ i]))
  | _ -> None

let split ss =
  let rec s_split n s = function
    | [] -> [(n, s)]
    | x::xs -> begin match take_comment x with
               | None -> s_split n (s ^ x) xs
               | Some n' -> (n, s)::(s_split n' "" xs) end in
  s_split "" "" ss |> List.tl

let input_line_op ic =
  try input_line ic |> String.trim |> (fun x -> Some x) with
    End_of_file -> begin close_in ic; None end

let rec read ic = match input_line_op ic with
  | None -> []
  | Some s -> if String.length s > 0 then s::(read ic) else read ic

let read_all ic = String.concat "\n" (read ic)

let file_hundle filename =
  let ic = open_in filename in
  read ic |> split

let json_clean s =
  let is_escape c = match c with
    | '"' | ':' | ',' | '[' | ']' | '{' | '}' | '`' | '\'' -> true
    | _ -> false in
  String.init (String.length s) (fun i -> if is_escape s.[i] then ' ' else s.[i])

let read_int_list ic = List.map int_of_string (read ic)

let call_query s =
  let hash = (Random.int 9999) in
  let tmp_qout = Printf.sprintf "tmp_qout%i.txt" hash in
  let tmp_qerr = Printf.sprintf "tmp_qerr%i.txt" hash in
  let command = Printf.sprintf "../query.native <<< \"%s\" > %s 2> %s" s tmp_qout tmp_qerr in

  let result = match Sys.command command with
    | 0 ->  Ok("ok")
    | i -> Error(Printf.sprintf "error(%i)" i) in

  let stdout = read_all (open_in tmp_qout) in
  let stderr = read_all (open_in tmp_qerr) in 

  let output = match result with
    | Ok(r) ->  stdout, r
    | Error(e) -> Printf.sprintf "\"error\": \"%s\"," (json_clean stderr), e in

    begin
      ignore(Sys.command (Printf.sprintf "rm %s" tmp_qout));
      ignore(Sys.command (Printf.sprintf "rm %s" tmp_qerr));
      output
    end

let call_main_with_timeout s time =

  let hash = (Random.int 9999) in
  let tmp_out = Printf.sprintf "tmp_out%i.txt" hash in
  let tmp_err = Printf.sprintf "tmp_err%i.txt" hash in
  let arg_convert_on = if convert_on then "-c" else "" in
  let arg_antichain_on = if antichain_on then "-a" else "" in
  let command = Printf.sprintf "timeout %s ../main.native %s %s <<< \"%s\" > %s 2> %s" time arg_convert_on arg_antichain_on s tmp_out tmp_err in

  let result = match Sys.command command with
    | 0 ->  Ok("ok")
    | 124 -> Ok("timeout")
    | i -> Error(Printf.sprintf "error(%i)" i) in

  let stdout = read_all (open_in tmp_out) in
  let stderr = read_all (open_in tmp_err) in 

  let output = match result with
    | Ok(r) ->  stdout, r
    | Error(e) -> Printf.sprintf "\"error\": \"%s\"," (json_clean stderr), e in

    begin
      ignore(Sys.command (Printf.sprintf "rm %s" tmp_out));
      ignore(Sys.command (Printf.sprintf "rm %s" tmp_err));
      output
    end

let () =
  let fs = file_hundle Sys.argv.(1) in
  let bl = if Sys.argv.(2) = "None" then []
           else read_int_list (open_in Sys.argv.(2)) in

  begin
    Format.fprintf Format.std_formatter "{@[" ;
    Format.fprintf Format.std_formatter "\"file\": \"%s\",@," Sys.argv.(1);
    Format.fprintf Format.std_formatter "\"timeoutconfig\": \"%s\",@," timeout;
    Format.fprintf Format.std_formatter "\"convert_on\": \"%b\",@," convert_on;
    Format.fprintf Format.std_formatter "\"antichain_on\": \"%b\",@," antichain_on;
    Format.fprintf Format.std_formatter "\"records\": [@[@,";
    List.iteri (fun i (_, s) ->
        if List.mem i bl then
          begin
            Format.fprintf Format.std_formatter "{@[" ;
            Format.fprintf Format.std_formatter "\"index\": %i,@," i;

            Format.fprintf Format.std_formatter "\"qstatus\": \"Skipped\",@," ;
            Format.fprintf Format.std_formatter "\"status\": \"Skipped\"@," ;

            if (succ i) = List.length fs then Format.fprintf Format.std_formatter "@]}@,"
            else Format.fprintf Format.std_formatter "@]},@,";

          end
        else
          begin
            Format.fprintf Format.std_formatter "{@[" ;
            Format.fprintf Format.std_formatter "\"index\": %i,@," i;
            let qo, qr = call_query s in
            Format.fprintf Format.std_formatter "%s@,\"qstatus\": \"%s\",@," qo qr;

            let o, r = call_main_with_timeout s timeout in
            Format.fprintf Format.std_formatter "%s@,\"status\": \"%s\"@," o r;
            if (succ i) = List.length fs then Format.fprintf Format.std_formatter "@]}@,"
            else Format.fprintf Format.std_formatter "@]},@,";
          end) fs;
    Format.fprintf Format.std_formatter "@]]@,";
    Format.fprintf Format.std_formatter "@]}@.";
  end
