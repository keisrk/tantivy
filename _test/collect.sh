#! /bin/bash
args=("$@")
in_f=${args[0]}

grep -n 'assert' $in_f | awk -F: '{print ($1, index($2, "assert")-1)}'
