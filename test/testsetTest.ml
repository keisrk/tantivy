open OUnit

let suite =
  "suite">:::
    [
    ]

let () =
  begin
    ignore(run_test_tt_main Tk5_test_d3_l500_v2.suite);
    ignore(run_test_tt_main Tk5_test_d4_l500_v2.suite);

    ignore(run_test_tt_main Tk5_test_d3_l500_v3.suite);
    ignore(run_test_tt_main Tk5_test_d4_l500_v3.suite);
  end
