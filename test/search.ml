open OUnit
(*
let testPostR test_ctxt =
  let lhs = Atmta.compose_reach Testset.sig3_char_op init_tri_r_p2d (snd (Atmta.compos_init init_tri_r_p2d)) in
  let rhs = [] in
  assert_equal lhs rhs
let testPostB test_ctxt =
  let lhs = Atmta.compose_reach Testset.sig3_char_op init_tri_b_p2d (snd (Atmta.compos_init init_tri_b_p2d)) in
  let rhs = [] in
  assert_equal lhs rhs
let testPostG test_ctxt =
  let lhs = Atmta.compose_reach Testset.sig3_char_op init_tri_g_p2d (snd (Atmta.compos_init init_tri_g_p2d)) in
  let rhs = [] in
  assert_equal lhs rhs
let testPostRBG test_ctxt =
  let lhs = Atmta.compose_reach Testset.sig3_char_op init_tri_and_rbg_p2d (snd (Atmta.compos_init init_tri_and_rbg_p2d)) in
  let rhs = [] in
  assert_equal lhs rhs
 *)
let suite =
  "suite">:::

    [
(*
      "testPostR">::testPostR ;
      "testPostB">::testPostB ;
      "testPostG">::testPostG ;
      "testPostRBG">::testPostRBG ;
 *)
    ]

let () =
  begin
    ignore(run_test_tt_main suite);
  end

(* BEGIN AND PART
(* Checked and Done *)

let lay_r_check = [(x, y, z)| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);       x +  2 * y - 3 * z = 2 ]
let lay_b_check = [(x, y, z)| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);   5 * x -  2 * y +     z = 3 ]
let lay_g_check = [(x, y, z)| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);   3 * x +      y + 2 * z = 1 ]

let tri_r_check = [(x, y, z)| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);   7 * x - 14 * y + 5 * z = -7 ]
let tri_b_check = [(x, y, z)| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31); -14 * x +  7 * y + 5 * z = -7 ]
let tri_g_check = [(x, y, z)| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);  14 * x + 14 * y +     z = 322]

let init_lay_and_r_b  = Atmta.AndI(lay_rd, lay_bd)
let init_lay_and_r_g  = Atmta.AndI(lay_rd, lay_gd)
let init_tri_and_r_b  = Atmta.AndI(tri_rd, tri_bd)
let init_tri_and_r_g  = Atmta.AndI(tri_rd, tri_gd)

let (lay_and_r_b, ilay_and_r_b) = Atmta.compose init_lay_and_r_b 
let (lay_and_r_g, ilay_and_r_g) = Atmta.compose init_lay_and_r_g 
let (tri_and_r_b, itri_and_r_b) = Atmta.compose init_tri_and_r_b 
let (tri_and_r_g, itri_and_r_g) = Atmta.compose init_tri_and_r_g 

let result_lay_and_r_b = Testset.check ws ws_dec (List.map Util.unwrap lay_and_r_b) ilay_and_r_b (Atmta.is_accept (lay_rf @ lay_bf)) ((fun tk a -> Presb.solve tk a = 0) lay_rtk)  
let result_lay_and_r_g = Testset.check ws ws_dec (List.map Util.unwrap lay_and_r_g) ilay_and_r_g (Atmta.is_accept (lay_rf @ lay_gf)) ((fun tk a -> Presb.solve tk a = 0) lay_rtk) 
let result_tri_and_r_b = Testset.check ws ws_dec (List.map Util.unwrap tri_and_r_b) itri_and_r_b (Atmta.is_accept (tri_rf @ tri_bf)) ((fun tk a -> Presb.solve tk a = 0) tri_rtk) 
let result_tri_and_r_g = Testset.check ws ws_dec (List.map Util.unwrap tri_and_r_g) itri_and_r_g (Atmta.is_accept (tri_rf @ tri_gf)) ((fun tk a -> Presb.solve tk a = 0) tri_rtk) 

let lay_r_and_b_check = [pos | pos <- lay_r_check; pos' <- lay_b_check; pos = pos']
let lay_r_and_g_check = [pos | pos <- lay_r_check; pos' <- lay_g_check; pos = pos']

let tri_r_and_b_check = [pos | pos <- tri_r_check; pos' <- tri_b_check; pos = pos']
let tri_r_and_g_check = [pos | pos <- tri_r_check; pos' <- tri_g_check; pos = pos']

   END AND PART *)

(* BEGIN AND PART

let test0 test_ctxt =
  let lhs = result_lay_and_r_b in
  let rhs = lay_r_and_b_check in
  assert_equal lhs rhs
let test1 test_ctxt =
  let lhs = result_lay_and_r_g in
  let rhs = lay_r_and_g_check in
  assert_equal lhs rhs
let test2 test_ctxt =
  let lhs = result_tri_and_r_b in
  let rhs = tri_r_and_b_check in
  assert_equal lhs rhs
let test3 test_ctxt =
  let lhs = result_tri_and_r_g in
  let rhs = tri_r_and_g_check in
  assert_equal lhs rhs 

   END AND PART *)

(* BEGIN AND PART
     "test0">:: test0 ;
      "test1">:: test1 ;
      "test2">:: test2 ;
      "test3">:: test3 ;
   END AND PART *)
