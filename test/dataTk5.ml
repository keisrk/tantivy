let tk5_test_d3_l500_v2_01 =
"
p 2 
and(exists(x1, eq(e2n4268, -17x0 + 17x1 = -34)),
     or(eq(e2n9653, -8x0 + 10x1 = -50), not(eq(e2n884, 5x0  -7x1 = 15))))
"
let tk5_test_d3_l500_v2_02 =
"
p 2 
or(not(exists(x1, eq(e2n9340, -5x0 + 2x1 = -80))),
    and(exists(x1, eq(e2n4749, 1x0  -5x1 = -64)), exists(x0,
         eq(e2n8449, -10x0 + 8x1 = 62))))
"
let tk5_test_d3_l500_v2_03 =
"
p 2 
or(not(and(eq(e2n299, -12x0 + 16x1 = 56), eq(e2n6793, -4x0  -4x1 = -56))),
    and(exists(x1, eq(e2n1769, -1x0  -5x1 = -37)),
         not(eq(e2n2665, -2x0  -14x1 = -86))))
"  
let tk5_test_d3_l500_v2_04 =
"
p 2 
exists(x1,
 and(and(eq(e2n4428, 3x0  -5x1 = -51), eq(e2n7924, -1x0  -5x1 = -61)),
      exists(x1, eq(e2n2068, 12x0  -14x1 = -62))))
"
let tk5_test_d3_l500_v2_05 =
"
p 2 
or(not(not(eq(e2n6774, 10x0  -14x1 = -42))),
    or(eq(e2n9785, -3x0 + 2x1 = -27),
        and(eq(e2n7977, -6x0  -3x1 = -57), eq(e2n4701, 3x0 + 1x1 = 28))))
"  
let tk5_test_d4_l500_v2_01 =
"
p 2 
and(or(or(or(eq(e2n4105, 6x0  -6x1 = 0), eq(e2n7327, -3x0 + 1x1 = -69)),
           not(eq(e2n3912, 1x0  -3x1 = -67))),
        not(not(eq(e2n1474, 15x0  -3x1 = 87)))),
     and(or(eq(e2n9026, -2x0 + 6x1 = 98), eq(e2n6211, -2x0 + 1x1 = -38)),
          exists(x1,
          and(eq(e2n9002, -6x0 + 15x1 = 24), eq(e2n9922, 15x0  -6x1 = -27)))))
"
let tk5_test_d4_l500_v2_02 =
"
p 2 
and(or(not(eq(e2n884, 5x0  -7x1 = 15)), exists(x1,
        and(eq(e2n9661, 7x0  -2x1 = 49), eq(e2n5154, -3x0 + 2x1 = 7)))),
     or(and(eq(e2n2352, 6x0  -10x1 = -22),
             not(eq(e2n7825, -2x0  -10x1 = -92))),
         and(eq(e2n1807, 8x0  -2x1 = -4), exists(x1,
              eq(e2n4679, 12x0  -9x1 = -87)))))
"
let tk5_test_d4_l500_v2_03 =
"
p 2 
and(or(exists(x0, not(eq(e2n645, 3x0  -2x1 = 44))),
        and(exists(x0, eq(e2n341, -2x0  -7x1 = -76)), exists(x1,
             eq(e2n767, -18x0 + 12x1 = 60)))),
     and(and(eq(e2n7327, -3x0 + 1x1 = -69),
              and(eq(e2n3003, 2x0 + 5x1 = 89), eq(e2n9367, 3x0  -5x1 = -73))),
          and(not(eq(e2n8866, 12x0  -8x1 = 0)), exists(x1,
               eq(e2n4480, -2x0 + 3x1 = 22)))))
"
let tk5_test_d4_l500_v2_04 =
"
p 2 
or(or(not(exists(x0, eq(e2n4269, -4x0 + 10x1 = 0))),
       and(and(eq(e2n7848, -5x0  -3x1 = -94), eq(e2n7200, -1x0  -7x1 = -51)),
            not(eq(e2n4252, -8x0 + 2x1 = -64)))),
    or(and(not(eq(e2n4167, -6x0 + 15x1 = 93)),
            or(eq(e2n1010, -8x0 + 10x1 = 52), eq(e2n2172, -6x0 + 1x1 = -9))),
        not(or(eq(e2n8910, 10x0  -15x1 = -15), eq(e2n376, -16x0 + 4x1 = -92)))))
"
let tk5_test_d4_l500_v2_05 =
"
p 2 
or(and(or(not(eq(e2n2068, 12x0  -14x1 = -62)), exists(x1,
           eq(e2n8910, 10x0  -15x1 = -15))),
        exists(x1,
        and(eq(e2n105, -7x0 + 3x1 = -74), eq(e2n6308, -9x0 + 15x1 = 84)))),
    and(or(eq(e2n5285, -1x0 + 2x1 = -10), exists(x1,
            eq(e2n2940, -10x0 + 4x1 = 6))),
         or(eq(e2n1395, -1x0  -1x1 = -48), exists(x1,
             eq(e2n6774, 10x0  -14x1 = -42)))))
"
let tk5_test_d3_l500_v3_01 =
"
p 3 
and(exists(x0, eq(e3n7931, -8x0  -3x1 + 7x2 = -28)),
     or(eq(e3n3652, 6x0 + 4x1  -10x2 = 66),
         not(eq(e3n6986, 8x0  -5x1 + 1x2 = 42))))
"

let tk5_test_d3_l500_v3_02 =
"
p 3 
or(not(exists(x1, eq(e3n7756, 14x0  -7x1  -7x2 = 42))),
    and(exists(x0, eq(e3n1136, -7x0 + 5x1 + 3x2 = -43)), exists(x1,
         eq(e3n4836, 4x0  -4x1  -5x2 = -95))))
"

let tk5_test_d3_l500_v3_03 =
"
p 3 
or(not(and(eq(e3n9799, 3x0  -1x1 + 10x2 = 81),
            eq(e3n4264, 4x0  -5x1  -2x2 = 24))),
    and(exists(x1, eq(e3n5747, 16x0  -12x1 + 8x2 = -92)),
         not(eq(e3n8077, 4x0  -1x1  -10x2 = -36))))
"

let tk5_test_d3_l500_v3_04 =
"
p 3 
exists(x0,
 and(and(eq(e3n3243, -18x0 + 18x1 + 12x2 = 96),
          eq(e3n4040, 8x0 + 2x1  -9x2 = -12)),
      exists(x1, eq(e3n7854, -4x0  -6x1 + 14x2 = -82))))
"

let tk5_test_d3_l500_v3_05 =
"
p 3 
or(not(not(eq(e3n3243, -18x0 + 18x1 + 12x2 = 96))),
    or(eq(e3n8972, 7x0  -7x1 + 4x2 = 27),
        and(eq(e3n3189, -8x0 + 2x1 + 12x2 = -64),
             eq(e3n3094, -5x0 + 6x1  -3x2 = 3))))
"
let tk5_test_d4_l500_v3_01 =
"
p 3 
and(or(or(or(eq(e3n2150, -18x0 + 4x1 + 6x2 = -14),
              eq(e3n1808, 12x0 + 9x1  -6x2 = 6)),
           not(eq(e3n3136, -6x0  -6x1 + 8x2 = 12))),
        not(not(eq(e3n3220, 1x0 + 8x1 + 1x2 = 72)))),
     and(or(eq(e3n9893, -8x0 + 16x1 + 6x2 = 52),
             eq(e3n3094, -5x0 + 6x1  -3x2 = 3)),
          exists(x2,
          and(eq(e3n6211, -2x0 + 16x1  -14x2 = -94),
               eq(e3n5289, -8x0  -8x1 + 10x2 = -50)))))
"
let tk5_test_d4_l500_v3_02 =
"
p 3 
and(or(not(eq(e3n3570, 7x0  -5x1 + 2x2 = 2)), exists(x2,
        and(eq(e3n4924, 7x0  -7x1  -1x2 = -87),
             eq(e3n9440, 6x0 + 5x1  -5x2 = 65)))),
     or(and(eq(e3n6607, 18x0  -14x1  -10x2 = -28),
             not(eq(e3n4420, -4x0 + 9x1  -3x2 = -10))),
         and(eq(e3n2486, 1x0  -4x1 + 10x2 = 66), exists(x0,
              eq(e3n2443, -3x0  -1x1 + 1x2 = -87)))))
"
let tk5_test_d4_l500_v3_03 =
"
p 3 
and(or(exists(x0, not(eq(e3n4264, 4x0  -5x1  -2x2 = 24))),
        and(exists(x1, eq(e3n3461, 6x0  -5x1  -6x2 = -19)), exists(x1,
             eq(e3n5614, -1x0  -2x1 + 3x2 = 20)))),
     and(and(eq(e3n69, 10x0  -10x1 + 2x2 = -46),
              and(eq(e3n7931, -8x0  -3x1 + 7x2 = -28),
                   eq(e3n7846, -9x0 + 1x1  -1x2 = -75))),
          and(not(eq(e3n2155, 12x0 + 4x1  -12x2 = -36)), exists(x2,
               eq(e3n6612, 6x0 + 8x1  -4x2 = 24)))))
"
let tk5_test_d4_l500_v3_04 =
"
p 3 
or(or(not(exists(x1, eq(e3n1745, -7x0 + 1x1 + 3x2 = -96))),
       and(and(eq(e3n1695, -3x0 + 9x1 + 2x2 = 69),
                eq(e3n3538, -1x0 + 3x1 + 1x2 = 99)),
            not(eq(e3n6245, -18x0 + 15x1  -6x2 = 15)))),
    or(and(not(eq(e3n7270, -7x0 + 10x1  -4x2 = -37)),
            or(eq(e3n9471, -4x0  -10x1 + 10x2 = -54),
                eq(e3n6937, 2x0  -4x1 + 1x2 = -15))),
        not(or(eq(e3n9845, -1x0 + 3x1  -6x2 = -55),
                eq(e3n5701, 12x0 + 2x1  -6x2 = -18)))))
"
let tk5_test_d4_l500_v3_05 =
"
p 3 
or(and(or(not(eq(e3n7854, -4x0  -6x1 + 14x2 = -82)), exists(x1,
           eq(e3n6733, -8x0  -16x1 + 18x2 = -56))),
        exists(x2,
        and(eq(e3n7495, -9x0  -1x1  -2x2 = -94),
             eq(e3n1174, 4x0 + 2x1  -16x2 = -40)))),
    and(or(eq(e3n4261, 9x0  -8x1  -6x2 = -68), exists(x0,
            eq(e3n7854, -4x0  -6x1 + 14x2 = -82))),
         or(eq(e3n2552, 6x0  -9x1  -3x2 = -96), exists(x0,
             eq(e3n5495, -9x0  -3x1 + 1x2 = -89)))))
"
