open OUnit

let (ws, ws_dec) = Testset.ws_len 3 5
let (tri_rd, tri_rf, tri_rtk)::(tri_bd, tri_bf, tri_btk)::(tri_gd, tri_gf, tri_gtk)::_ = Testset.tri ()

let init_tri_r_p2d  = Atmta.DetI(Atmta.PrjI(2, tri_rd))
let init_tri_b_p2d  = Atmta.DetI(Atmta.PrjI(2, tri_bd))
let init_tri_g_p2d  = Atmta.DetI(Atmta.PrjI(2, tri_gd))
let init_tri_andA_rb_p2d = Atmta.AndI(init_tri_r_p2d, init_tri_b_p2d)
let init_tri_andB_rb_p2d = Atmta.DetI(Atmta.PrjI(2, Atmta.AndI(tri_rd, tri_bd)))

let init_tri_andA_rbg_p2d = Atmta.AndI(Atmta.AndI(init_tri_r_p2d, init_tri_b_p2d), init_tri_g_p2d)
let init_tri_andB_rbg_p2d = Atmta.DetI(Atmta.PrjI(2, Atmta.AndI(Atmta.AndI(tri_rd, tri_bd), tri_gd)))

let init_tri_andA_rbg_p2d = Atmta.AndI(Atmta.AndI(init_tri_r_p2d, init_tri_b_p2d), init_tri_g_p2d)
let init_tri_andB_rbg_p2d = Atmta.DetI(Atmta.PrjI(2, Atmta.AndI(Atmta.AndI(tri_rd, tri_bd), tri_gd)))

let rslt_tri_r_p2d = Testset.check ws ws_dec init_tri_r_p2d (Atmta.is_accept (init_tri_r_p2d)) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tri_b_p2d = Testset.check ws ws_dec init_tri_b_p2d (Atmta.is_accept (init_tri_b_p2d)) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tri_g_p2d = Testset.check ws ws_dec init_tri_g_p2d (Atmta.is_accept (init_tri_g_p2d)) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tri_andA_rb_p2d = Testset.check ws ws_dec init_tri_andA_rb_p2d (Atmta.is_accept (init_tri_andA_rb_p2d))
                             |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tri_andB_rb_p2d = Testset.check ws ws_dec init_tri_andB_rb_p2d (Atmta.is_accept (init_tri_andB_rb_p2d))
                             |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let rslt_tri_andA_rbg_p2d = Testset.check ws ws_dec init_tri_andA_rbg_p2d (Atmta.is_accept (init_tri_andA_rbg_p2d))
                             |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tri_andB_rbg_p2d = Testset.check ws ws_dec init_tri_andB_rbg_p2d (Atmta.is_accept (init_tri_andB_rbg_p2d))
                             |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let tri_r_p2d_check = [[|x; y; z|]| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);List.exists (fun c ->    7 * x - 14 * y + 5 * c = -7 ) (Util.range 0 31)]
let tri_b_p2d_check = [[|x; y; z|]| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);List.exists (fun c ->  -14 * x +  7 * y + 5 * c = -7 ) (Util.range 0 31)]
let tri_g_p2d_check = [[|x; y; z|]| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);List.exists (fun c ->   14 * x + 14 * y +     c = 322) (Util.range 0 31)]
let tri_andA_rb_p2d_check = [ pos | pos <- tri_r_p2d_check; pos' <- tri_b_p2d_check; pos = pos']
let tri_andB_rb_p2d_check = [[|x; y; z|]| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);
                                          List.exists (fun c -> (  7 * x - 14 * y + 5 * c = -7)&&(-14 * x +  7 * y + 5 * c = -7) ) (Util.range 0 31)]
                            |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let tri_andA_rbg_p2d_check = List.fold_left (fun acc i -> if (List.mem i tri_b_p2d_check) && (List.mem i tri_g_p2d_check) then i::acc else acc) [] tri_r_p2d_check
                             |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let tri_andB_rbg_p2d_check = [[|x; y; z|]| x <- (Util.range 0 31); y <- (Util.range 0 31); z <- (Util.range 0 31);
                              List.exists (fun c ->    (7 * x - 14 * y + 5 * c = -7) &&
                                                         (-14 * x +  7 * y + 5 * c = -7) &&
                                                           (14 * x + 14 * y +     c = 322) ) (Util.range 0 31)]
                             |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let test_tri_r_p2d test_ctxt =
  let lhs = rslt_tri_r_p2d in
  let rhs = tri_r_p2d_check in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end
let test_tri_b_p2d test_ctxt =
  let lhs = rslt_tri_b_p2d in
  let rhs = tri_b_p2d_check in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end
let test_tri_g_p2d test_ctxt =
  let lhs = rslt_tri_g_p2d in
  let rhs = tri_g_p2d_check in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end
let test_tri_andA_rb_p2d test_ctxt =
  let lhs = rslt_tri_andA_rb_p2d in
  let rhs = tri_andA_rb_p2d_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end
let test_tri_andB_rb_p2d test_ctxt =
  let lhs = rslt_tri_andB_rb_p2d in
  let rhs = tri_andB_rb_p2d_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tri_andA_rbg_p2d test_ctxt =
  let lhs = rslt_tri_andA_rbg_p2d in
  let rhs = tri_andA_rbg_p2d_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end
let test_tri_andB_rbg_p2d test_ctxt =
  let lhs = rslt_tri_andB_rbg_p2d in
  let rhs = tri_andB_rbg_p2d_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let suite =
  "suite">:::

    [
      "test_tri_r_p2d">::test_tri_r_p2d ;
      "test_tri_b_p2d">::test_tri_b_p2d ;
      "test_tri_g_p2d">::test_tri_g_p2d ;
      "test_tri_andA_rb_p2d">::test_tri_andA_rb_p2d ;
      "test_tri_andB_rb_p2d">::test_tri_andB_rb_p2d ;
      "test_tri_andA_rbg_p2d">::test_tri_andA_rbg_p2d ;
      "test_tri_andB_rbg_p2d">::test_tri_andB_rbg_p2d ;
    ]

let () =
  begin
    ignore(run_test_tt_main suite);
  end
