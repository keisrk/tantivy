open OUnit
open DataTk5

(*  Header  *)
let (ws3, ws_dec3) = Testset.ws_len 3 5
let of_string s = Parser.problem Lexer.problem (Lexing.from_string s)
let set3d = [[|x0; x1; x2|] | x0 <- BoundPresb.Var3.range; x1 <- BoundPresb.Var3.range; x2 <- BoundPresb.Var3.range]
(*  Header  *)

let rslt_tk5_test_d3_l500_v3_01 =
  let term = Testset.of_problem (of_string tk5_test_d3_l500_v3_01) in
  Testset.check ws3 ws_dec3 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tk5_test_d3_l500_v3_02 =
  let term = Testset.of_problem (of_string tk5_test_d3_l500_v3_02) in
  Testset.check ws3 ws_dec3 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tk5_test_d3_l500_v3_03 =
  let term = Testset.of_problem (of_string tk5_test_d3_l500_v3_03) in
  Testset.check ws3 ws_dec3 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tk5_test_d3_l500_v3_04 =
  let term = Testset.of_problem (of_string tk5_test_d3_l500_v3_04) in
  Testset.check ws3 ws_dec3 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let rslt_tk5_test_d3_l500_v3_05 =
  let term = Testset.of_problem (of_string tk5_test_d3_l500_v3_05) in
  Testset.check ws3 ws_dec3 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let tk5_test_d3_l500_v3_01_check =
  let problem = of_string tk5_test_d3_l500_v3_01 in
  [ x | x <- set3d; (BoundPresb.Var3.instance problem) x.(0) x.(1) x.(2)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let tk5_test_d3_l500_v3_02_check =
  let problem = of_string tk5_test_d3_l500_v3_02 in
  [ x | x <- set3d; (BoundPresb.Var3.instance problem) x.(0) x.(1) x.(2)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let tk5_test_d3_l500_v3_03_check =
  let problem = of_string tk5_test_d3_l500_v3_03 in
  [ x | x <- set3d; (BoundPresb.Var3.instance problem) x.(0) x.(1) x.(2)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let tk5_test_d3_l500_v3_04_check =
  let problem = of_string tk5_test_d3_l500_v3_04 in
  [ x | x <- set3d; (BoundPresb.Var3.instance problem) x.(0) x.(1) x.(2)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)
let tk5_test_d3_l500_v3_05_check =
  let problem = of_string tk5_test_d3_l500_v3_05 in
  [ x | x <- set3d; (BoundPresb.Var3.instance problem) x.(0) x.(1) x.(2)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d3_l500_v3_01 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d3_l500_v3_01)) in
  Testset.check ws3 ws_dec3 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d3_l500_v3_02 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d3_l500_v3_02)) in
  Testset.check ws3 ws_dec3 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d3_l500_v3_03 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d3_l500_v3_03)) in
  Testset.check ws3 ws_dec3 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d3_l500_v3_04 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d3_l500_v3_04)) in
  Testset.check ws3 ws_dec3 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d3_l500_v3_05 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d3_l500_v3_05)) in
  Testset.check ws3 ws_dec3 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let test_tk5_test_d3_l500_v3_01 test_ctxt =
  let lhs = rslt_tk5_test_d3_l500_v3_01 in
  let rhs = tk5_test_d3_l500_v3_01_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d3_l500_v3_02 test_ctxt =
  let lhs = rslt_tk5_test_d3_l500_v3_02 in
  let rhs = tk5_test_d3_l500_v3_02_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d3_l500_v3_03 test_ctxt =
  let lhs = rslt_tk5_test_d3_l500_v3_03 in
  let rhs = tk5_test_d3_l500_v3_03_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d3_l500_v3_04 test_ctxt =
  let lhs = rslt_tk5_test_d3_l500_v3_04 in
  let rhs = tk5_test_d3_l500_v3_04_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d3_l500_v3_05 test_ctxt =
  let lhs = rslt_tk5_test_d3_l500_v3_05 in
  let rhs = tk5_test_d3_l500_v3_05_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d3_l500_v3_01 test_ctxt =
  let lhs = cv_rslt_tk5_test_d3_l500_v3_01 in
  let rhs = tk5_test_d3_l500_v3_01_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d3_l500_v3_02 test_ctxt =
  let lhs = cv_rslt_tk5_test_d3_l500_v3_02 in
  let rhs = tk5_test_d3_l500_v3_02_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d3_l500_v3_03 test_ctxt =
  let lhs = cv_rslt_tk5_test_d3_l500_v3_03 in
  let rhs = tk5_test_d3_l500_v3_03_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d3_l500_v3_04 test_ctxt =
  let lhs = cv_rslt_tk5_test_d3_l500_v3_04 in
  let rhs = tk5_test_d3_l500_v3_04_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d3_l500_v3_05 test_ctxt =
  let lhs = cv_rslt_tk5_test_d3_l500_v3_05 in
  let rhs = tk5_test_d3_l500_v3_05_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let suite =
  "suite">:::

    [
      "test_tk5_test_d3_l500_v3_01">::test_tk5_test_d3_l500_v3_01 ;
      "test_tk5_test_d3_l500_v3_02">::test_tk5_test_d3_l500_v3_02 ;
      "test_tk5_test_d3_l500_v3_03">::test_tk5_test_d3_l500_v3_03 ;
      "test_tk5_test_d3_l500_v3_04">::test_tk5_test_d3_l500_v3_04 ;
      "test_tk5_test_d3_l500_v3_05">::test_tk5_test_d3_l500_v3_05 ;

      "test_cv_tk5_test_d3_l500_v3_01">::test_cv_tk5_test_d3_l500_v3_01 ;
      "test_cv_tk5_test_d3_l500_v3_02">::test_cv_tk5_test_d3_l500_v3_02 ;
      "test_cv_tk5_test_d3_l500_v3_03">::test_cv_tk5_test_d3_l500_v3_03 ;
      "test_cv_tk5_test_d3_l500_v3_04">::test_cv_tk5_test_d3_l500_v3_04 ;
      "test_cv_tk5_test_d3_l500_v3_05">::test_cv_tk5_test_d3_l500_v3_05 ;

    ]
