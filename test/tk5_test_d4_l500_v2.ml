open OUnit
open DataTk5

(*  Header  *)
let (ws2, ws_dec2) = Testset.ws_len 2 5
let of_string s = Parser.problem Lexer.problem (Lexing.from_string s)
let set2d = [[|x0; x1|] | x0 <- BoundPresb.Var2.range; x1 <- BoundPresb.Var2.range]
(*  Header  *)

let rslt_tk5_test_d4_l500_v2_01 =
  let term = Testset.of_problem (of_string tk5_test_d4_l500_v2_01) in
  Testset.check ws2 ws_dec2 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
                                        
let tk5_test_d4_l500_v2_01_check =
  let problem = of_string tk5_test_d4_l500_v2_01 in
  [ x | x <- set2d; (BoundPresb.Var2.instance problem) x.(0) x.(1)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let rslt_tk5_test_d4_l500_v2_02 =
  let term = Testset.of_problem (of_string tk5_test_d4_l500_v2_02) in
  Testset.check ws2 ws_dec2 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
                                        
let tk5_test_d4_l500_v2_02_check =
  let problem = of_string tk5_test_d4_l500_v2_02 in
  [ x | x <- set2d; (BoundPresb.Var2.instance problem) x.(0) x.(1)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let rslt_tk5_test_d4_l500_v2_03 =
  let term = Testset.of_problem (of_string tk5_test_d4_l500_v2_03) in
  Testset.check ws2 ws_dec2 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
                                        
let tk5_test_d4_l500_v2_03_check =
  let problem = of_string tk5_test_d4_l500_v2_03 in
  [ x | x <- set2d; (BoundPresb.Var2.instance problem) x.(0) x.(1)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let rslt_tk5_test_d4_l500_v2_04 =
  let term = Testset.of_problem (of_string tk5_test_d4_l500_v2_04) in
  Testset.check ws2 ws_dec2 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
                                        
let tk5_test_d4_l500_v2_04_check =
  let problem = of_string tk5_test_d4_l500_v2_04 in
  [ x | x <- set2d; (BoundPresb.Var2.instance problem) x.(0) x.(1)]
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let rslt_tk5_test_d4_l500_v2_05 =
  let term = Testset.of_problem (of_string tk5_test_d4_l500_v2_05) in
  Testset.check ws2 ws_dec2 term (Atmta.is_accept term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
                                        
let tk5_test_d4_l500_v2_05_check =
  let problem = of_string tk5_test_d4_l500_v2_05 in
  [ x | x <- set2d; (BoundPresb.Var2.instance problem) x.(0) x.(1)]

let cv_rslt_tk5_test_d4_l500_v2_01 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d4_l500_v2_01)) in
  Testset.check ws2 ws_dec2 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d4_l500_v2_02 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d4_l500_v2_02)) in
  Testset.check ws2 ws_dec2 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d4_l500_v2_03 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d4_l500_v2_03)) in
  Testset.check ws2 ws_dec2 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d4_l500_v2_04 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d4_l500_v2_04)) in
  Testset.check ws2 ws_dec2 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let cv_rslt_tk5_test_d4_l500_v2_05 =
  let cv_term = Testset.of_problem ((fun (Presb.Problem(vars, f)) -> Presb.Problem(vars, Enumerate.cv_t f)) (of_string tk5_test_d4_l500_v2_05)) in
  Testset.check ws2 ws_dec2 cv_term (Atmta.is_accept cv_term) |> Util.of_list (fun x y -> compare x y |> Util.of_int)
  |> Util.of_list (fun x y -> compare x y |> Util.of_int)

let test_tk5_test_d4_l500_v2_01 test_ctxt =
  let lhs = rslt_tk5_test_d4_l500_v2_01 in
  let rhs = tk5_test_d4_l500_v2_01_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d4_l500_v2_02 test_ctxt =
  let lhs = rslt_tk5_test_d4_l500_v2_02 in
  let rhs = tk5_test_d4_l500_v2_02_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d4_l500_v2_03 test_ctxt =
  let lhs = rslt_tk5_test_d4_l500_v2_03 in
  let rhs = tk5_test_d4_l500_v2_03_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d4_l500_v2_04 test_ctxt =
  let lhs = rslt_tk5_test_d4_l500_v2_04 in
  let rhs = tk5_test_d4_l500_v2_04_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_tk5_test_d4_l500_v2_05 test_ctxt =
  let lhs = rslt_tk5_test_d4_l500_v2_05 in
  let rhs = tk5_test_d4_l500_v2_05_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d4_l500_v2_01 test_ctxt =
  let lhs = cv_rslt_tk5_test_d4_l500_v2_01 in
  let rhs = tk5_test_d4_l500_v2_01_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d4_l500_v2_02 test_ctxt =
  let lhs = cv_rslt_tk5_test_d4_l500_v2_02 in
  let rhs = tk5_test_d4_l500_v2_02_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d4_l500_v2_03 test_ctxt =
  let lhs = cv_rslt_tk5_test_d4_l500_v2_03 in
  let rhs = tk5_test_d4_l500_v2_03_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d4_l500_v2_04 test_ctxt =
  let lhs = cv_rslt_tk5_test_d4_l500_v2_04 in
  let rhs = tk5_test_d4_l500_v2_04_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let test_cv_tk5_test_d4_l500_v2_05 test_ctxt =
  let lhs = cv_rslt_tk5_test_d4_l500_v2_05 in
  let rhs = tk5_test_d4_l500_v2_05_check  in
  begin Format.fprintf Format.std_formatter "LHS %i; RHS %i\n" (List.length lhs) (List.length rhs); assert_equal lhs rhs end

let suite =
  "suite">:::

    [
      "test_tk5_test_d4_l500_v2_01">::test_tk5_test_d4_l500_v2_01 ;
      "test_tk5_test_d4_l500_v2_02">::test_tk5_test_d4_l500_v2_02 ;
      "test_tk5_test_d4_l500_v2_03">::test_tk5_test_d4_l500_v2_03 ;
      "test_tk5_test_d4_l500_v2_04">::test_tk5_test_d4_l500_v2_04 ;
      "test_tk5_test_d4_l500_v2_05">::test_tk5_test_d4_l500_v2_05 ;

      "test_cv_tk5_test_d4_l500_v2_01">::test_cv_tk5_test_d4_l500_v2_01 ;
      "test_cv_tk5_test_d4_l500_v2_02">::test_cv_tk5_test_d4_l500_v2_02 ;
      "test_cv_tk5_test_d4_l500_v2_03">::test_cv_tk5_test_d4_l500_v2_03 ;
      "test_cv_tk5_test_d4_l500_v2_04">::test_cv_tk5_test_d4_l500_v2_04 ;
      "test_cv_tk5_test_d4_l500_v2_05">::test_cv_tk5_test_d4_l500_v2_05 ;

    ]
