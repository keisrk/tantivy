let to_tuple s =
  let s' = String.trim s in
  let n = String.index s' ' ' in
  (String.sub s' 0 n |> int_of_string, String.sub s' (succ n) (String.length s' - (succ n)) |> int_of_string)

let cnt = Array.make 1 0

let of_file_name fn ln_loc =
  let ic = open_in fn in

  begin
    try
      while true do
        Array.set ln_loc cnt.(0) ((to_tuple (input_line ic)), "");
        Array.set cnt 0 (succ cnt.(0));
      done
    with End_of_file -> close_in ic;
  end

let print ln_loc =
  Array.iter  (fun ((a, b), s) -> Format.fprintf Format.std_formatter "%i, %i, %s\n" a b s) (Array.sub ln_loc 0 (cnt.(0)))
