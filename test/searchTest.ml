open OUnit
open DataTk5

(*  Header  *)
let of_string s = Parser.problem Lexer.problem (Lexing.from_string s)
(*  Header  *)

let suite =
  "suite">:::

    [
    ]

let () =
  begin
    ignore(run_test_tt_main suite);
  end
