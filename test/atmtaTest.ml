open OUnit

let ln_loc = Array.make 20 ((0, 0), "")

let file = "atmta_assert.txt"
let source_file = "src/atmta.ml"

let st8s = List.map (fun x -> Atmta.Prim x) ["c"; "a"; "b"; "a"; "d"]
let su_st8s = Util.of_list Atmta.St8.cmp st8s

let test0 test_ctxt =
  let index = 0 in
  let err () = Atmta.St8.(Atmta.Prim "e" <:: st8s) in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test1 test_ctxt =
  let index = 1 in
  let err () = Atmta.St8.(st8s <@ su_st8s) in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test2 test_ctxt =
  let index = 2 in
  let err () = Atmta.St8.(su_st8s <@ st8s) in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test8 test_ctxt =
  let index = 8 in
  let err () = Atmta.for_all2 (=) (Array.of_list st8s) (Array.of_list su_st8s) in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let suite =
  "suite">:::
    [
      "test0">:: test0 ;
      "test1">:: test1 ;
      "test2">:: test2 ;
      "test8">:: test8
    ]

let () =
  begin
    Collect.of_file_name file ln_loc;
    ignore(run_test_tt_main suite);
    Collect.print ln_loc;
  end
