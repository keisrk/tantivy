open OUnit

(* 
  val Collect.of_file_name : string -> int * int array -> int * int array
  collect.sh saves line, char pos tuples of assertion in the source file.
*)

let ln_loc = Array.make 20 ((0, 0), "")

let file = "util_assert.txt"
let source_file = "src/util.ml"

let nms = Util.range 0 100
let cmp x y = compare x y |> Util.of_int
let mod_i i x = if x mod i = 0 then Some x else None
let p x y = if y = 0 then true else x mod y = 0 

let test0 test_ctxt =
  let index = 0 in
  let err () =
    Util.tr_of_ls [[]; [0; 1]; [0; 1]] in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test1 test_ctxt =
  let index = 1 in
  let err () =
    let dat = Util.filter_map (fun i -> Some i) nms in
    Util.( 3 <:: dat )in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test2 test_ctxt =
  let index = 2 in
  let err () =
    let dat = Util.filter_map (fun i -> Some i) nms in
    Util.set_compare cmp dat nms in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end
(*
let test3 test_ctxt =
  let index = 3 in
  let err () =
    let dat = Util.filter_map (fun i -> Some i) nms in
    Util.set_compare cmp nms dat in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test4 test_ctxt =
  let index = 4 in
  let err () =
    let dat = Util.filter_map (fun i -> Some i) nms in
    Util.union cmp dat nms in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test5 test_ctxt =
  let index = 5 in
  let err () =
    let dat = Util.filter_map (fun i -> Some i) nms in
    Util.union cmp nms dat in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test13 test_ctxt =
  let index = 13 in
  let err () =
    let p x y = match (x, y) with |(Some a, Some b) -> a = b | _ -> false in
    Util.classify cmp p (List.map (fun i -> if i < 5 then None else Some i) nms) in

  begin (* boilerplate *)
    let ((ln, loc), _) = ln_loc.(index) in
    Array.set ln_loc index ((ln, loc), "Done");
    assert_raises (Assert_failure (source_file, ln, loc)) err
  end

let test14 test_ctxt =
  let md3 = Util.filter_map (mod_i 3) nms |> Util.of_list cmp in
  let md6 = Util.filter_map (mod_i 6) nms |> Util.of_list cmp in

  let lhs = Util.inter cmp md3 md6 in
  let rhs = md6 in
  assert_equal lhs rhs
 *)
let suite =
  "suite">:::
    [
      "test0">:: test0 ;
      "test1">:: test1 ;
      "test2">:: test2 ;
      (*      "test3">:: test3 ;*)
      (*      "test4">:: test4 ;*)
      (*      "test5">:: test5 ;*)
      (*      "test13">:: test13;*)
      (*      "test14">:: test14*)
    ]

let () =
  begin
    Collect.of_file_name file ln_loc;
    ignore(run_test_tt_main suite);
    Collect.print ln_loc;
  end
