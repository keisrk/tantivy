cwd=$(pwd)
now=$(date +%m%d%k)
function c_false_a_false {
    cd $cwd/_experiments01 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d6_l100_v4.txt None | python -m json.tool > ~/Documents/log/d6_l100_v4$now.json &
}                                                                                             
function c_true_a_false {                                                                     
    cd $cwd/_experiments03 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d6_l100_v4.txt None -c | python -m json.tool > ~/Documents/log/d6_l100_v4_c$now.json &
}                                                                                             
function c_false_a_true {                                                                     
    cd $cwd/_experiments05 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d6_l100_v4.txt None -a | python -m json.tool > ~/Documents/log/d6_l100_v4_a$now.json &
}                                                                                             
function c_true_a_true {                                                                      
    cd $cwd/_experiments07 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d6_l100_v4.txt None -c -a | python -m json.tool > ~/Documents/log/d6_l100_v4_ca$now.json &
}
c_false_a_false
c_true_a_false
c_false_a_true
c_true_a_true
