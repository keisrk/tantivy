cwd=$(pwd)
now=$(date +%m%d%k)
function c_false_a_false {
    cd $cwd/_experiments00 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v2.txt $cwd/_test/input_files/blkl_d3_l500_v2.txt | python -m json.tool > ~/Documents/log/full_test_d3_l500_v2$now.json &
    cd $cwd/_experiments01 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v2.txt $cwd/_test/input_files/blkl_d4_l500_v2.txt | python -m json.tool > ~/Documents/log/full_test_d4_l500_v2$now.json &
    cd $cwd/_experiments02 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v3.txt $cwd/_test/input_files/blkl_d3_l500_v3.txt | python -m json.tool > ~/Documents/log/full_test_d3_l500_v3$now.json &
    cd $cwd/_experiments03 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v3.txt $cwd/_test/input_files/blkl_d4_l500_v3.txt | python -m json.tool > ~/Documents/log/full_test_d4_l500_v3$now.json &
    cd $cwd/_experiments04 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v4.txt $cwd/_test/input_files/blkl_d3_l500_v4.txt | python -m json.tool > ~/Documents/log/full_test_d3_l500_v4$now.json &
    cd $cwd/_experiments05 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v4.txt $cwd/_test/input_files/blkl_d4_l500_v4.txt | python -m json.tool > ~/Documents/log/full_test_d4_l500_v4$now.json
}
function c_true_a_false {
    cd $cwd/_experiments00 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v2.txt $cwd/_test/input_files/blkl_d3_l500_v2.txt -c | python -m json.tool > ~/Documents/log/full_test_d3_l500_v2_c$now.json &
    cd $cwd/_experiments01 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v2.txt $cwd/_test/input_files/blkl_d4_l500_v2.txt -c | python -m json.tool > ~/Documents/log/full_test_d4_l500_v2_c$now.json &
    cd $cwd/_experiments02 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v3.txt $cwd/_test/input_files/blkl_d3_l500_v3.txt -c | python -m json.tool > ~/Documents/log/full_test_d3_l500_v3_c$now.json &
    cd $cwd/_experiments03 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v3.txt $cwd/_test/input_files/blkl_d4_l500_v3.txt -c | python -m json.tool > ~/Documents/log/full_test_d4_l500_v3_c$now.json &
    cd $cwd/_experiments04 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v4.txt $cwd/_test/input_files/blkl_d3_l500_v4.txt -c | python -m json.tool > ~/Documents/log/full_test_d3_l500_v4_c$now.json &
    cd $cwd/_experiments05 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v4.txt $cwd/_test/input_files/blkl_d4_l500_v4.txt -c | python -m json.tool > ~/Documents/log/full_test_d4_l500_v4_c$now.json
}    
function c_false_a_true {
    cd $cwd/_experiments00 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v2.txt $cwd/_test/input_files/blkl_d3_l500_v2.txt -a | python -m json.tool > ~/Documents/log/full_test_d3_l500_v2_a$now.json &
    cd $cwd/_experiments01 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v2.txt $cwd/_test/input_files/blkl_d4_l500_v2.txt -a | python -m json.tool > ~/Documents/log/full_test_d4_l500_v2_a$now.json &
    cd $cwd/_experiments02 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v3.txt $cwd/_test/input_files/blkl_d3_l500_v3.txt -a | python -m json.tool > ~/Documents/log/full_test_d3_l500_v3_a$now.json &
    cd $cwd/_experiments03 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v3.txt $cwd/_test/input_files/blkl_d4_l500_v3.txt -a | python -m json.tool > ~/Documents/log/full_test_d4_l500_v3_a$now.json &
    cd $cwd/_experiments04 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v4.txt $cwd/_test/input_files/blkl_d3_l500_v4.txt -a | python -m json.tool > ~/Documents/log/full_test_d3_l500_v4_a$now.json &
    cd $cwd/_experiments05 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v4.txt $cwd/_test/input_files/blkl_d4_l500_v4.txt -a | python -m json.tool > ~/Documents/log/full_test_d4_l500_v4_a$now.json
}
function c_true_a_true {
    cd $cwd/_experiments00 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v2.txt $cwd/_test/input_files/blkl_d3_l500_v2.txt -c -a | python -m json.tool > ~/Documents/log/full_test_d3_l500_v2_ca$now.json &
    cd $cwd/_experiments01 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v2.txt $cwd/_test/input_files/blkl_d4_l500_v2.txt -c -a | python -m json.tool > ~/Documents/log/full_test_d4_l500_v2_ca$now.json &
    cd $cwd/_experiments02 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v3.txt $cwd/_test/input_files/blkl_d3_l500_v3.txt -c -a | python -m json.tool > ~/Documents/log/full_test_d3_l500_v3_ca$now.json &
    cd $cwd/_experiments03 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v3.txt $cwd/_test/input_files/blkl_d4_l500_v3.txt -c -a | python -m json.tool > ~/Documents/log/full_test_d4_l500_v3_ca$now.json &
    cd $cwd/_experiments04 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d3_l500_v4.txt $cwd/_test/input_files/blkl_d3_l500_v4.txt -c -a | python -m json.tool > ~/Documents/log/full_test_d3_l500_v4_ca$now.json &
    cd $cwd/_experiments05 && ocaml $cwd/_test/test_script.ml $cwd/_test/input_files/test_d4_l500_v4.txt $cwd/_test/input_files/blkl_d4_l500_v4.txt -c -a | python -m json.tool > ~/Documents/log/full_test_d4_l500_v4_ca$now.json
}
c_false_a_false
c_true_a_false
c_false_a_true
c_true_a_true
